/**
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.yuyh.imgsel;

import com.yuyh.imgsel.utile.EventHelper;
import com.yuyh.library.imgsel.config.ISListConfig;
import com.yuyh.library.imgsel.single.BasicAbility;
import com.yuyh.library.imgsel.single.ResultAbility;
import com.yuyh.library.imgsel.ui.ISListActivity;
import com.yuyh.library.imgsel.ui.ImgSelFragment;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.agp.components.*;
import ohos.agp.render.Path;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ExampleOhosTest {
    @Test
    public void testBundleName() {
        final String actualBundleName = AbilityDelegatorRegistry.getArguments().getTestBundleName();
        assertEquals("com.yuyh.imgsel", actualBundleName);
    }

    @Test
    public void testPicture() {
        MainActivity ability = EventHelper.startAbility(MainActivity.class);
        Button ButtonSelect = (Button) ability.findComponentById(ResourceTable.Id_multiSelect);
        gotoSleep(1000);
        EventHelper.triggerClickEvent(ability, ButtonSelect);
        Assert.assertNotNull(ISListConfig.getInstance());
        gotoSleep(3000);
    }

    @Test
    public void testSelect() {
        testPicture();
        ImgSelFragment ImgSelFragment = (ImgSelFragment) AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility();
        ListContainer listContainer = (ListContainer) ImgSelFragment.findComponentById(ResourceTable.Id_recycler);
        for (int i = 1; i <= 5; i++) {
            StackLayout superCheckBox = (StackLayout) listContainer.getComponentAt(i).findComponentById(ResourceTable.Id_checkView);
            gotoSleep(500);
            EventHelper.triggerClickEvent(ImgSelFragment, superCheckBox);
        }
        Button buttonOk = (Button) ImgSelFragment.findComponentById(ResourceTable.Id_btn_ok);
        assertTrue(buttonOk.getText().equals("确定(5/9)"));
    }


    @Test
    public void testFolder() {
        MainActivity ability = EventHelper.startAbility(MainActivity.class);
        Button ButtonSelect = (Button) ability.findComponentById(ResourceTable.Id_multiSelect);
        gotoSleep(1000);
        EventHelper.triggerClickEvent(ability, ButtonSelect);
        gotoSleep(3000);
        ImgSelFragment imgSelFragment = (ImgSelFragment) AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility();
        gotoSleep(5000);
        Text dir = (Text) imgSelFragment.findComponentById(ResourceTable.Id_tv_dir);
        gotoSleep(5000);
        EventHelper.triggerClickEvent(imgSelFragment, dir);
        gotoSleep(5000);
        Assert.assertNotNull(imgSelFragment.mFolders.size());
    }

    @Test
    public void testPreViewMove() {
        testPicture();
        ImgSelFragment ImgSelFragment = (ImgSelFragment) AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility();
        ListContainer listContainer = (ListContainer) ImgSelFragment.findComponentById(ResourceTable.Id_recycler);
        Image componentAt = (Image) listContainer.getComponentAt(1).findComponentById(ResourceTable.Id_iv_thumb);
        EventHelper.triggerClickEvent(ImgSelFragment, componentAt);
        gotoSleep(3000);
        ISListActivity imagePreviewBaseAbility = (ISListActivity) AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility();
        PageSlider viewPager=null;
        for (int i = 0; i < 2; i++) {
            viewPager = (PageSlider) imagePreviewBaseAbility.findComponentById(ResourceTable.Id_viewpager);
            gotoSleep(1000);
            Path path = new Path();
            path.moveTo(1000f, 300f);
            path.lineTo(10f, 300f);
            EventHelper.inputSwipe(imagePreviewBaseAbility, viewPager, path, 200);
            gotoSleep(1000);
        }
        Assert.assertTrue(viewPager.getCurrentSlidingState()==PageSlider.SLIDING_STATE_IDLE);
    }

    @Test
    public void testPreViewSelect() {
        testPicture();
        ImgSelFragment ImgSelFragment = (ImgSelFragment) AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility();
        ListContainer listContainer = (ListContainer) ImgSelFragment.findComponentById(ResourceTable.Id_recycler);
        Image componentAt = (Image) listContainer.getComponentAt(1).findComponentById(ResourceTable.Id_iv_thumb);
        EventHelper.triggerClickEvent(ImgSelFragment, componentAt);
        gotoSleep(3000);
        ISListActivity imagePreviewBaseAbility = (ISListActivity) AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility();
        PageSlider viewPager = (PageSlider) imagePreviewBaseAbility.findComponentById(ResourceTable.Id_viewpager);
        Checkbox superCheckBox = (Checkbox) imagePreviewBaseAbility.findComponentById(ResourceTable.Id_cb_check);
        EventHelper.triggerClickEvent(imagePreviewBaseAbility, superCheckBox);
        gotoSleep(2000);
        Button buttonOk = (Button) imagePreviewBaseAbility.findComponentById(ResourceTable.Id_btn_ok);
        Assert.assertTrue(buttonOk.getText().equals("确定(1/9)"));
        gotoSleep(2000);
    }


    @Test
    public void testSiniglePicture() {
        MainActivity ability = EventHelper.startAbility(MainActivity.class);
        Button buttonSingle = (Button) ability.findComponentById(ResourceTable.Id_single);
        gotoSleep(1000);
        EventHelper.triggerClickEvent(ability, buttonSingle);
        gotoSleep(2000);
        Assert.assertTrue(ISListConfig.getInstance().isShowCamera());
    }

    @Test
    public void testSinigleCrop() {
        testSiniglePicture();
        ImgSelFragment imgSelFragment = (ImgSelFragment) AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility();
        ListContainer listContainer = (ListContainer) imgSelFragment.findComponentById(ResourceTable.Id_recycler);
        Image image = (Image) listContainer.getComponentAt(2).findComponentById(ResourceTable.Id_iv_thumb);
        EventHelper.triggerClickEvent(imgSelFragment, image);
        gotoSleep(2000);
        BasicAbility cropAbility = (BasicAbility) AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility();
        Button buttonCrop = (Button) cropAbility.findComponentById(ResourceTable.Id_buttonDone);
        EventHelper.triggerClickEvent(cropAbility, buttonCrop);
        gotoSleep(2000);
        ResultAbility resultAbility = (ResultAbility) AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility();
        Assert.assertNotNull(resultAbility.image.getPixelMap());
        gotoSleep(2000);
    }

    @Test
    public void testSinigleFolder() {
        MainActivity ability = EventHelper.startAbility(MainActivity.class);
        Button buttonsing = (Button) ability.findComponentById(ResourceTable.Id_single);
        gotoSleep(1000);
        EventHelper.triggerClickEvent(ability, buttonsing);
        gotoSleep(3000);
        ImgSelFragment imgSelFragment = (ImgSelFragment) AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility();
        gotoSleep(3000);
        Text dir = (Text) imgSelFragment.findComponentById(ResourceTable.Id_tv_dir);
        gotoSleep(3000);
        EventHelper.triggerClickEvent(imgSelFragment, dir);
        gotoSleep(3000);
        Assert.assertTrue(ISListConfig.getInstance().isMultiMode());
    }


    private void gotoSleep(long l) {
        try {
            Thread.sleep(l);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}