/**
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.yuyh.imgsel;

import com.yuyh.library.imgsel.bean.MyImage;
import com.yuyh.library.imgsel.common.ImageLoader;
import com.yuyh.library.imgsel.config.ISListConfig;
import com.yuyh.library.imgsel.utils.ISFileProvider;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;

public class ExampleTest {

    @Test
    public void testneedCamera() {
        ISListConfig ISListConfig = ISListConfig.getInstance();
        ISListConfig.setImageLoader(new ISFileProvider());
        ISListConfig.needCamera(true);
        Assert.assertTrue(ISListConfig.isShowCamera());
    }

    @Test
    public void testmaxNum() {
        ISListConfig ISListConfig = ISListConfig.getInstance();
        ISListConfig.setImageLoader(new ISFileProvider());
        ISListConfig.maxNum(9);
        Assert.assertTrue(
                ISListConfig.getSelectLimit() == 9);
    }

    @Test
    public void testmultiSelect() {
        ISListConfig ISListConfig = ISListConfig.getInstance();
        ISListConfig.setImageLoader(new ISFileProvider());
        ISListConfig.multiSelect(true);
        Assert.assertTrue(ISListConfig.isMultiMode());
    }

    @Test
    public void testSingle() {
        ISListConfig ISListConfig = ISListConfig.getInstance();
        ISListConfig.setImageLoader(new ISFileProvider());
        ISListConfig.multiSelect(false);
        Assert.assertTrue(!ISListConfig.isMultiMode());
    }

    @Test
    public void testImageLoadr() {
        ISListConfig.getInstance().setImageLoader(new ISFileProvider());
        ImageLoader imageLoader = ISListConfig.getInstance().getImageLoader();
        Assert.assertTrue(imageLoader instanceof ISFileProvider);
    }

    @Test
    public void testtakeImageFile() {
        ISListConfig instance = ISListConfig.getInstance();
        instance.setTakeImageFile(new File("soure"));
        Assert.assertTrue(instance.getTakeImageFile() instanceof File);
    }
    @Test
    public void testCurrentImageFolderPosition() {
        ISListConfig instance = ISListConfig.getInstance();
        instance.setCurrentImageFolderPosition(0);
        Assert.assertTrue(instance.getCurrentImageFolderPosition()==0);
    }

    @Test
    public void testisSelect(){
        ISListConfig instance= ISListConfig.getInstance();
        instance.isSelect(new MyImage());
        instance.addSelectedImageItem(1,new MyImage(),true);
        Assert.assertTrue(instance.getSelectImageCount()!=0);
    }

}
