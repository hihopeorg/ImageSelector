package com.yuyh.imgsel;


import com.yuyh.library.imgsel.bean.MyImage;
import com.yuyh.library.imgsel.config.ISListConfig;
import com.yuyh.library.imgsel.config.ISCameraConfig;
import com.yuyh.library.imgsel.ui.ImgSelFragment;
import com.yuyh.library.imgsel.utils.ISFileProvider;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.*;
import ohos.bundle.IBundleManager;

import java.util.ArrayList;

import static com.yuyh.library.imgsel.config.ISListConfig.EXTRA_RESULT_ITEMS;
import static com.yuyh.library.imgsel.config.ISListConfig.RESULT_CODE_ITEMS;

/**
 * https://github.com/smuyyh/ImageSelector
 *
 * @author yuyh.
 * @date 2016/8/5.
 */
public class MainActivity extends Ability {

    private static final int REQUEST_LIST_CODE = 0;
    private static final int REQUEST_CAMERA_CODE = 1;

    private Text tvResult;
    private Button multiSelect;
    Button single;
    private Button camera;
    public static int RESULT_OK = 100;
    private Image image;
    private ListContainer listContainer;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_activity_main);
        listContainer = (ListContainer) findComponentById(ResourceTable.Id_list_container);
        multiSelect = (Button) findComponentById(ResourceTable.Id_multiSelect);
        image = (Image) findComponentById(ResourceTable.Id_image);
        camera = (Button) findComponentById(ResourceTable.Id_camera);
        single = (Button) findComponentById(ResourceTable.Id_single);
        Multiselect();
        Single();
        Camera();
    }

    public void Multiselect() {
        multiSelect.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                ISListConfig isListConfig = ISListConfig.getInstance();
                isListConfig.setImageLoader(new ISFileProvider());   //设置图片加载器
                isListConfig.needCamera(true);                      //显示拍照按钮
                isListConfig.maxNum(9);
                isListConfig.multiSelect(true);
                //选中数量限制
                Intent intent = new Intent();
                Operation operation = new Intent.OperationBuilder()
                        .withBundleName(getBundleName())
                        .withAbilityName(ImgSelFragment.class.getName())
                        .build();
                intent.setOperation(operation);
                startAbilityForResult(intent, 123);
            }
        });

    }

    public void Single() {
        single.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                ISListConfig isListConfig = ISListConfig.getInstance();
                isListConfig.setImageLoader(new ISFileProvider());   //设置图片加载器
                isListConfig.needCamera(true);                      //显示拍照按钮
                isListConfig.maxNum(9);
                isListConfig.multiSelect(false);
                Operation operation = new Intent.OperationBuilder()
                        .withBundleName(getBundleName())
                        .withAbilityName(ImgSelFragment.class.getName())
                        .build();
                Intent intent = new Intent();
                intent.setOperation(operation);
                startAbility(intent);
            }
        });

    }

    private static final int MY_PERMISSIONS_REQUEST_CAMERA = 100;

    public void Camera() {
        camera.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                applyPermission();//权限申请
                Intent intent = new Intent();
                Operation operation = new Intent.OperationBuilder()
                        .withBundleName(getBundleName())
                        .withAbilityName(ISCameraConfig.class.getName())
                        .build();
                intent.setOperation(operation);
                startAbility(intent);
            }
        });
    }

    private void applyPermission() {
        if (verifySelfPermission("ohos.permission.CAMERA") != IBundleManager.PERMISSION_GRANTED &&
                verifySelfPermission("ohos.permission.MEDIA_LOCATION") != IBundleManager.PERMISSION_GRANTED &&
                verifySelfPermission("ohos.permission.WRITE_MEDIA") != IBundleManager.PERMISSION_GRANTED
        ) {
            // 应用未被授予权限
            if (canRequestPermission("ohos.permission.CAMERA") &&
                    canRequestPermission("ohos.permission.MEDIA_LOCATION") &&
                    canRequestPermission("ohos.permission.WRITE_MEDIA")
            ) {
                // 是否可以申请弹框授权(首次申请或者用户未选择禁止且不再提示)
                requestPermissionsFromUser(
                        new String[]{"ohos.permission.CAMERA", "ohos.permission.MEDIA_LOCATION"
                                , "ohos.permission.WRITE_MEDIA"}, MY_PERMISSIONS_REQUEST_CAMERA);
            } else {
                // 显示应用需要权限的理由，提示用户进入设置授权
            }
        }
    }


    @Override
    protected void onAbilityResult(int requestCode, int resultCode, Intent data) {
        super.onAbilityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_CODE_ITEMS) {
            ArrayList<MyImage> list = data.getSerializableParam(EXTRA_RESULT_ITEMS);
            listContainer.setItemProvider(new BaseItemProvider() {
                @Override
                public int getCount() {
                    return list.size();
                }

                @Override
                public Object getItem(int i) {
                    return list.get(i);
                }

                @Override
                public long getItemId(int i) {
                    return i;
                }

                @Override
                public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
                    final Component cpt;
                    if (component == null) {
                        cpt = LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_item_text, componentContainer, false);
                    } else {
                        cpt = component;
                    }
                    MyImage sampleItem = list.get(i);
                    Text text = (Text) cpt.findComponentById(ResourceTable.Id_text);
                    text.setText(sampleItem.uriSchema);
                    return cpt;
                }
            });
            MyImage myImage = list.get(0);
            ISListConfig isListConfig = ISListConfig.getInstance();
            isListConfig.getImageLoader().displayImage(MainActivity.this, myImage.uriSchema, image, myImage.width,myImage.height);
        }
    }
}
