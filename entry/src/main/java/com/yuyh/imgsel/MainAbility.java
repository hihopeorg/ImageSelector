package com.yuyh.imgsel;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class MainAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(MainAbility.class.getName());
        addActionRoute(Intent.ACTION_QUERY_WEATHER, MainActivity.class.getName());
    }
}
