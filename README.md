# ImageSelector

**本项目是基于开源项目ImageSelector进行ohos化的移植和开发的，可以通过项目标签以及github地址（https://github.com/smuyyh/ImageSelector ）追踪到原项目版本**

## 项目介绍

- 项目名称：ImageSelector
- 所属系列：ohos的第三方组件适配移植
- 功能：是给应用提供简化读取相册，照片单选和多选，以及照片裁剪等功能
- 项目移植状态：完成
- 调用差异：无
- 项目作者和维护人：hihope
- 联系方式：hihope@hoperun.com
- 原项目Doc地址：https://github.com/smuyyh/ImageSelector
- 原项目基线版本: V2.0.5 sha1:3487a2b3850b14fc567b154841ec24880a3a529f
- 编程语言：Java
- 外部库依赖：无

## 效果展示

![](ImageSelector.gif);

## 安装教程

#### 方法一：

1. 编译ImageSelector的har包library.har。

2. 启动 DevEco Studio，将编译的har包，在工程目录新建“libs”存放到下面。

3. 在工程级别下的build.gradle文件中添加:

  ```
 allprojects {
     repositories {
         maven {
             url 'https://repo.huaweicloud.com/repository/maven/'
         }
         maven {
             url 'https://developer.huawei.com/repo/'
         }
 		jcenter()
     flatDir {
         dir'../libs/'
     }
     }
 }
  ```

4.在需要引用har包的目录下
   ```
implementation fileTree(dir: 'libs', include: ['*.jar','*.har'])
api(name:"imageloader-release",ext:"har")
   ```

#### 方法二：

  1. 在工程的build.gradle的allprojects中，添加HAR所在的Maven仓地址:

```
 repositories {
     maven {
         url 'http://106.15.92.248:8081/repository/Releases/' 
     }
 }
```

  2. 在应用模块的build.gradle的dependencies闭包中，添加如下代码:

```
 dependencies {
     implementation 'com.yuyh.imgsel:imageSelector:1.0.1'
 }
```

## 使用说明

#### 1.获取单例对象
```
 SListConfig ISListConfig = SListConfig.getInstance();
```
##### 1)设置多选模式

```
 SListConfig ISListConfig = SListConfig.getInstance();
  ISListConfig.setImageLoader(new SelectImageLoader());       //设置图片加载器
                               ISListConfig.needCamera(true); //显示拍照按钮
                               ISListConfig.maxNum(9);        //最多选取几张图片
                               ISListConfig.multiSelect(true);//多选模式
```
##### 2)设置单选模式

```
   SListConfig ISListConfig = SListConfig.getInstance();
                ISListConfig.setImageLoader(new SelectImageLoader());    //设置图片加载器
                ISListConfig.needCamera(false);                           //隐藏拍照按钮
                ISListConfig.maxNum(9);                                  //最多选取几张图片
                ISListConfig.multiSelect(false);                          //单选模式
```


##### 3)截图

```
 mCropView.load(mSourceUri)
                .initialFrameRect(mFrameRect)// 矩形大小
                .useThumbnail(true)           //缩略图
                .execute(mLoadCallback);//开始裁剪
```
##### 4)拍照截图
```
    Intent intent=new Intent();
                Operation operation = new Intent.OperationBuilder()
                        .withBundleName(getBundleName())
                        .withAbilityName(SCameraActivity.class.getName())
                        .build();
                intent.setOperation(operation);
                startAbility(intent);
```

## 版本迭代

- v1.0.1


## 版权和许可信息

 Apache license version 2.0
