package com.yuyh.library.imgsel.adapter;


import com.yuyh.library.imgsel.config.ISListConfig;
import com.yuyh.library.imgsel.ResourceTable;
import com.yuyh.library.imgsel.bean.Folder;
import com.yuyh.library.imgsel.utils.FileUtils;
import com.yuyh.library.imgsel.utils.DisplayUtils;
import ohos.aafwk.ability.Ability;
import ohos.agp.components.*;

import java.util.ArrayList;
import java.util.List;


public class FolderListAdapter extends BaseItemProvider {

    private ISListConfig ISListConfig;
    private Ability mActivity;
    private LayoutScatter mInflater;
    private int mImageSize;
    private List<Folder> folders;
    private int lastSelected = 0;

    public FolderListAdapter(Ability ability, List<Folder> folders) {
        mActivity = ability;
        if (folders != null && folders.size() > 0) this.folders = folders;
        else this.folders = new ArrayList<>();

        ISListConfig = ISListConfig.getInstance();
        mImageSize = DisplayUtils.getImageItemWidth(mActivity);
        mInflater = (LayoutScatter)LayoutScatter.getInstance(ability) ;
    }

    public void refreshData(List<Folder> folders) {
        if (folders != null && folders.size() > 0) this.folders = folders;
        else this.folders.clear();
        notifyDataChanged();
    }

    @Override
    public int getCount() {
        return folders.size();
    }

    @Override
    public Folder getItem(int position) {
        return folders.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Component getComponent(int position, Component component, ComponentContainer componentContainer) {
        ViewHolder holder;
        if (component == null) {
            component = mInflater.parse(ResourceTable.Layout_adapter_folder_list_item, componentContainer, false);
            holder = new ViewHolder(component);
        } else {
            holder = (ViewHolder) component.getTag();
        }

        Folder folder = getItem(position);
        holder.folderName.setText(folder.name);
        holder.imageCount.setText(FileUtils.getString(mActivity,ResourceTable.String_ip_folder_image_count, folder.images.size()));
        ISListConfig.getImageLoader().displayImage(mActivity, folder.cover.uriSchema, holder.cover, mImageSize, mImageSize);

        if (lastSelected == position) {
            holder.folderCheck.setVisibility(Component.VISIBLE);
        } else {
            holder.folderCheck.setVisibility(Component.INVISIBLE);
        }

        return component;
    }


    public void setSelectIndex(int i) {
        if (lastSelected == i) {
            return;
        }
        lastSelected = i;
        notifyDataChanged();
    }

    public int getSelectIndex() {
        return lastSelected;
    }

    private class ViewHolder {
        Image cover;
        Text folderName;
        Text imageCount;
        Image folderCheck;

        public ViewHolder(Component view) {
            cover = (Image) view.findComponentById(ResourceTable.Id_iv_cover);
            folderName = (Text) view.findComponentById(ResourceTable.Id_tv_folder_name);
            imageCount = (Text) view.findComponentById(ResourceTable.Id_tv_image_count);
            folderCheck = (Image) view.findComponentById(ResourceTable.Id_iv_folder_check);
            view.setTag(this);
        }
    }
}
