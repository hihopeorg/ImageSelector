package com.yuyh.library.imgsel.config;

import com.yuyh.library.imgsel.bean.Folder;
import com.yuyh.library.imgsel.bean.MyImage;
import com.yuyh.library.imgsel.common.ImageLoader;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.app.Context;
import ohos.app.Environment;
import ohos.media.image.PixelMap;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class ISListConfig {

    public static final String TAG = ISListConfig.class.getSimpleName();
    public static final int REQUEST_CODE_TAKE = 1001;
    public static final int REQUEST_CODE_CROP = 1002;
    public static final int REQUEST_CODE_PREVIEW = 1003;
    public static final int RESULT_CODE_ITEMS = 1004;
    public static final int RESULT_CODE_BACK = 1005;
    public static final int RESULT_OK = 1001;
    public static final int RESULT_CANCELED = 1002;

    public static final String EXTRA_RESULT_ITEMS = "extra_result_items";
    public static final String EXTRA_SELECTED_IMAGE_POSITION = "selected_image_position";
    public static final String EXTRA_IMAGE_ITEMS = "extra_image_items";
    public static final String EXTRA_FROM_ITEMS = "extra_from_items";
    public static final String EXTRA_TARGET_FILE_PATH = "extra_target_file_path";

    private boolean multiMode = true;    //图片选择模式
    private int selectLimit = 9;         //最大选择图片数量

    private boolean showCamera = true;   //显示相机


    private int focusWidth = 280;         //焦点框的宽度
    private int focusHeight = 280;        //焦点框的高度
    private ImageLoader imageLoader;     //图片加载器
    private File cropCacheFolder;
    private File takeImageFile;
    public PixelMap cropBitmap;

    private ArrayList<MyImage> mSelectedImages = new ArrayList<>();   //选中的图片集合
    private List<Folder> mFolders;      //所有的图片文件夹
    private int mCurrentImageFolderPosition = 0;  //当前选中的文件夹位置 0表示所有图片
    private List<OnImageSelectedListener> mImageSelectedListeners;          // 图片选中的监听回调

    private static ISListConfig mInstance;
    private static Class takePhotoAbilityClass;

    private ISListConfig() {
    }

    public static ISListConfig getInstance() {
        if (mInstance == null) {
            synchronized (ISListConfig.class) {
                if (mInstance == null) {
                    mInstance = new ISListConfig();
                }
            }
        }
        return mInstance;
    }

    public static void setTakePhotoAbility(Class clz) {
       takePhotoAbilityClass = clz;
    }

    public boolean isMultiMode() {
        return multiMode;
    }

    public void multiSelect(boolean multiMode) {
        this.multiMode = multiMode;
    }

    public int getSelectLimit() {
        return selectLimit;
    }

    public void maxNum(int selectLimit) {
        this.selectLimit = selectLimit;
    }


    public boolean isShowCamera() {
        return showCamera;
    }

    public void needCamera(boolean showCamera) {
        this.showCamera = showCamera;
    }


    public File getTakeImageFile() {
        return takeImageFile;
    }

    public  void setTakeImageFile(File takeImageFile) {
        this.takeImageFile = takeImageFile;
    }

    public File getCropCacheFolder(Context context) {
        if (cropCacheFolder == null) {
            cropCacheFolder = new File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES) + "/sListConfig/cropTemp/");
        }
        return cropCacheFolder;
    }

    public void setCropCacheFolder(File cropCacheFolder) {
        this.cropCacheFolder = cropCacheFolder;
    }

    public ImageLoader getImageLoader() {
        return imageLoader;
    }

    public void setImageLoader(ImageLoader imageLoader) {
        this.imageLoader = imageLoader;
    }


    public List<Folder> getImageFolders() {
        return mFolders;
    }

    public void setImageFolders(List<Folder> folders) {
        mFolders = folders;
    }

    public int getCurrentImageFolderPosition() {
        return mCurrentImageFolderPosition;
    }

    public void setCurrentImageFolderPosition(int mCurrentSelectedImageSetPosition) {
        mCurrentImageFolderPosition = mCurrentSelectedImageSetPosition;
    }

    public ArrayList<MyImage> getCurrentImageFolderItems() {
        return mFolders.get(mCurrentImageFolderPosition).images;
    }

    public boolean isSelect(MyImage item) {
        return mSelectedImages.contains(item);
    }

    public int getSelectImageCount() {
        if (mSelectedImages == null) {
            return 0;
        }
        return mSelectedImages.size();
    }

    public ArrayList<MyImage> getSelectedImages() {
        return mSelectedImages;
    }

    public void clearSelectedImages() {
        if (mSelectedImages != null) mSelectedImages.clear();
    }

    public void clear() {
        if (mImageSelectedListeners != null) {
            mImageSelectedListeners.clear();
            mImageSelectedListeners = null;
        }
        if (mFolders != null) {
            mFolders.clear();
            mFolders = null;
        }
        if (mSelectedImages != null) {
            mSelectedImages.clear();
        }
        mCurrentImageFolderPosition = 0;
    }

    /**
     * 拍照
     *
     */
    public void takePicture(Ability ability) {
        Operation operation = new Intent.OperationBuilder()
                .withDeviceId("")
                .withBundleName(takePhotoAbilityClass.getPackage().getName())
                .withAbilityName(takePhotoAbilityClass.getName())
                .build();
        //打开预览
        Intent itent = new Intent();
        itent.setOperation(operation);
        ability.startAbilityForResult(itent, REQUEST_CODE_PREVIEW);
    }

    /**
     * 根据系统时间、前缀、后缀产生一个文件
     *
     */
    public static File createFile(File folder, String prefix, String suffix) {
        if (!folder.exists() || !folder.isDirectory()) {
            boolean mkdirs = folder.mkdirs();
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.CHINA);
        String filename = prefix + dateFormat.format(new Date(System.currentTimeMillis())) + suffix;
        return new File(folder, filename);
    }


    /**
     * 图片选中的监听
     */
    public interface OnImageSelectedListener {
        void onImageSelected(int position, MyImage item, boolean isAdd);
    }

    public  void addOnImageSelectedListener(OnImageSelectedListener l) {
        if (mImageSelectedListeners == null) mImageSelectedListeners = new ArrayList<>();
        mImageSelectedListeners.add(l);
    }

    public void removeOnImageSelectedListener(OnImageSelectedListener l) {
        if (mImageSelectedListeners == null) return;
        mImageSelectedListeners.remove(l);
    }

    public void addSelectedImageItem(int position, MyImage item, boolean isAdd) {
        if (isAdd) mSelectedImages.add(item);
        else mSelectedImages.remove(item);
        notifyImageSelectedChanged(position, item, isAdd);
    }

    public void setSelectedImages(ArrayList<MyImage> selectedImages) {
        if (selectedImages == null) {
            return;
        }
        this.mSelectedImages = selectedImages;
    }

    private void notifyImageSelectedChanged(int position, MyImage item, boolean isAdd) {
        if (mImageSelectedListeners == null) return;
        for (OnImageSelectedListener l : mImageSelectedListeners) {
            l.onImageSelected(position, item, isAdd);
        }
    }
}