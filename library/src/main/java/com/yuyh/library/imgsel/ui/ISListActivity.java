package com.yuyh.library.imgsel.ui;


import com.yuyh.library.imgsel.ResourceTable;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.window.dialog.ToastDialog;
import ohos.bundle.IBundleManager;


public class ISListActivity extends Ability {

    private ToastDialog dialog;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        getWindow().setStatusBarColor(ResourceTable.Color_ip_color_primary_dark);
    }

    public void showToast(String toastText) {
        if (dialog == null) {
            dialog = new ToastDialog(this);
        }
        if (dialog.isShowing()) {
            dialog.hide();
        }
        dialog.setText(toastText);
        dialog.setDuration(2_000);

        dialog.show();
    }
}
