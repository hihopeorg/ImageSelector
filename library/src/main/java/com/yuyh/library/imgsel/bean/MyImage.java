package com.yuyh.library.imgsel.bean;

import ohos.utils.Parcel;
import ohos.utils.Sequenceable;
import ohos.utils.net.Uri;

import java.io.Serializable;


public class MyImage implements Serializable, Sequenceable {

    public String name;       //图片的名字
    public String uriSchema;       //图片的路径
    public long size;         //图片的大小
    public int width;         //图片的宽度
    public int height;        //图片的高度
    public String mimeType;   //图片的类型
    public long addTime;      //图片的创建时间
    public Uri uri;

    public Uri getUri() {
        return uri;
    }

    public void setUri(Uri uri) {
        this.uri = uri;
    }


    @Override
    public boolean equals(Object o) {
        if (o instanceof MyImage) {
            MyImage item = (MyImage) o;
            return this.uriSchema.equalsIgnoreCase(item.uriSchema);
        }
        return super.equals(o);
    }

    public MyImage() {
    }

    protected MyImage(Parcel in) {
        this.name = in.readString();
        this.uriSchema = in.readString();
        this.size = in.readLong();
        this.width = in.readInt();
        this.height = in.readInt();
        this.mimeType = in.readString();
        this.addTime = in.readLong();
    }

    public static final Producer<MyImage> PRODUCER = new Producer<MyImage>() {
        @Override
        public MyImage createFromParcel(Parcel source) {
            return new MyImage(source);
        }
    };

    @Override
    public boolean marshalling(Parcel parcel) {
        parcel.writeString(this.name);
        parcel.writeString(this.uriSchema);
        parcel.writeLong(this.size);
        parcel.writeInt(this.width);
        parcel.writeInt(this.height);
        parcel.writeString(this.mimeType);
        parcel.writeLong(this.addTime);
        return true;
    }

    @Override
    public boolean unmarshalling(Parcel in) {
        this.name = in.readString();
        this.uriSchema = in.readString();
        this.size = in.readLong();
        this.width = in.readInt();
        this.height = in.readInt();
        this.mimeType = in.readString();
        this.addTime = in.readLong();
        return true;
    }

    @Override
    public String toString() {
        return "ImageItem{" +
                "name='" + name + '\'' +
                ", path='" + uriSchema + '\'' +
                ", size=" + size +
                ", width=" + width +
                ", height=" + height +
                ", mimeType='" + mimeType + '\'' +
                ", addTime=" + addTime +
                '}';
    }
}
