package com.yuyh.library.imgsel.utils;

import ohos.aafwk.ability.Ability;
import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;
import ohos.data.usage.DataUsage;
import ohos.data.usage.MountState;

public class DisplayUtils {


    public static int getImageItemWidth(Ability ability) {

        DisplayManager displayManager = DisplayManager.getInstance();
        Display display = displayManager.getDefaultDisplay(ability).get();

        int screenWidth = display.getAttributes().width;
        int densityDpi = display.getAttributes().densityDpi;
        int cols = screenWidth / densityDpi;
        cols = cols < 3 ? 3 : cols;
        int columnSpace = (int) (2 * display.getAttributes().densityPixels);
        return (screenWidth - columnSpace * (cols - 1)) / cols;
    }

    /**
     * 判断SD是否可用
     * @return 是否有sd卡
     */
    public static boolean existSDCard() {
        return DataUsage.getDiskMountedStatus().equals(MountState.DISK_MOUNTED);
    }

    public static int vp2px(int vp, Context mContext) {
        return (int)(mContext.getResourceManager().getDeviceCapability().screenDensity / 160 * vp);
    }


    public static int[] getScreenSize(Context context) {
        DisplayManager displayManager = DisplayManager.getInstance();
        Display display = displayManager.getDefaultDisplay(context).get();

        int screenWidth = display.getAttributes().width;
        int screenHeith = display.getAttributes().height;
        return new int[]{screenWidth, screenHeith};
    }
}
