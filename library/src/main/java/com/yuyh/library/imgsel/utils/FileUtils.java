package com.yuyh.library.imgsel.utils;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.ElementScatter;
import ohos.agp.components.element.PixelMapElement;
import ohos.app.Context;
import ohos.global.resource.*;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;

import java.io.IOException;
import java.util.Optional;

public class FileUtils {
    private static final String TAG = "ResUtil";

    private static final String CITY_ID_ATTR = "cityId_";

    private static final String STRING_ID_ATTR = "String_";

    private FileUtils() {
    }


    public static String getPathById(Context context, int id) {
        String path = "";
        if (context == null) {

            return path;
        }
        ResourceManager manager = context.getResourceManager();
        if (manager == null) {

            return path;
        }
        try {
            path = manager.getMediaPath(id);
        } catch (IOException | NotExistException | WrongTypeException e) {
            return path;
        }
        return path;
    }

    public static int getColor(Context context, int id) {
        int result = 0;
        if (context == null) {

            return result;
        }
        ResourceManager manager = context.getResourceManager();
        if (manager == null) {

            return result;
        }
        try {
            result = manager.getElement(id).getColor();
        } catch (IOException | WrongTypeException | NotExistException e) {
            return result;
        }
        return result;
    }

    public static String getString(Context context, int id) {
        String result = "";
        if (context == null) {

            return result;
        }
        ResourceManager manager = context.getResourceManager();
        if (manager == null) {

            return result;
        }
        try {
            result = manager.getElement(id).getString();
        } catch (IOException | WrongTypeException | NotExistException e) {
            return result;
        }
        return result;
    }
    public static String getString(Context context, int id, Object... format) {

        return String.format(getString(context, id), format);
    }
    public static Optional<PixelMap> getPixelMap(Context context, int id) {
        String path = getPathById(context, id);
        if (path == null || path.length() <= 0) {

            return Optional.empty();
        }
        RawFileEntry assetManager = context.getResourceManager().getRawFileEntry(path);
        ImageSource.SourceOptions options = new ImageSource.SourceOptions();
        options.formatHint = "image/png";
        ImageSource.DecodingOptions decodingOptions = new ImageSource.DecodingOptions();
        try {
            Resource asset = assetManager.openRawFile();
            ImageSource source = ImageSource.create(asset, options);
            return Optional.ofNullable(source.createPixelmap(decodingOptions));
        } catch (IOException e) {
            return Optional.empty();
        }

    }
    public static PixelMapElement getPixelMapDrawable(Context context, int resId) {
        Optional<PixelMap> optional = getPixelMap(context, resId);
        return optional.map(PixelMapElement::new).orElse(null);
    }

    public static Element getElement(Context context, int resId) {
        return ElementScatter.getInstance(context).parse(resId);
    }
    public static <T extends Component> T findComponentById(Component view, int id) {
        if (view == null) {
            return null;
        }
        return (T) view.findComponentById(id);
    }

    public static int getColorValue(AttrSet attrSet, String name, int defaultValue) {
        try {
            int value = attrSet.getAttr(name).get().getColorValue().getValue();
            if (value != 0) {
                return value;
            }
        } catch (Exception e) {
            return defaultValue;
        }
        return defaultValue;
    }

    public static int getIntValue(AttrSet attrSet, String name, int defaultValue) {
        try {
            int value = attrSet.getAttr(name).get().getIntegerValue();
            if (value != 0) {
                return value;
            }
        } catch (Exception e) {
            return defaultValue;
        }
        return defaultValue;
    }
}

