/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Issei Aoki
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.yuyh.library.imgsel.single;


import com.yuyh.library.imgsel.single.callback.LoadCallback;
import io.reactivex.Completable;
import ohos.agp.utils.RectFloat;
import ohos.utils.net.Uri;

public class LoadRequest {

    private float initialFrameScale;
    private RectFloat initialFrameRect;
    private boolean useThumbnail;
    private CropImageView cropImageView;
    private Uri uri;

    public LoadRequest(CropImageView cropImageView, Uri uri) {
        this.cropImageView = cropImageView;
        this.uri = uri;
    }

    public LoadRequest initialFrameScale(float initialFrameScale) {
        this.initialFrameScale = initialFrameScale;
        return this;
    }

    public LoadRequest initialFrameRect(RectFloat initialFrameRect) {
        this.initialFrameRect = initialFrameRect;
        return this;
    }

    public LoadRequest useThumbnail(boolean useThumbnail) {
        this.useThumbnail = useThumbnail;
        return this;
    }

    public void execute(LoadCallback callback) {
        if (initialFrameRect == null) {
            cropImageView.setInitialFrameScale(initialFrameScale);
        }
        cropImageView.loadAsync(uri, useThumbnail, initialFrameRect, callback);
    }

    public Completable executeAsCompletable() {
        if (initialFrameRect == null) {
            cropImageView.setInitialFrameScale(initialFrameScale);
        }
        return cropImageView.loadAsCompletable(uri, useThumbnail, initialFrameRect);
    }
}
