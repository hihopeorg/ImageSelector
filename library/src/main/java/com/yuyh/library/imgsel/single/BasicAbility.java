/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Issei Aoki
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.yuyh.library.imgsel.single;

import com.yuyh.library.imgsel.ResourceTable;

import com.yuyh.library.imgsel.single.callback.CropCallback;
import com.yuyh.library.imgsel.single.callback.LoadCallback;
import com.yuyh.library.imgsel.single.callback.SaveCallback;
import ohos.aafwk.ability.fraction.FractionAbility;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Component;
import ohos.agp.utils.RectFloat;
import ohos.bundle.IBundleManager;
import ohos.media.image.PixelMap;
import ohos.utils.net.Uri;

public class BasicAbility extends FractionAbility {

    public static Intent createIntent(Uri uri) {
        Intent intent = new Intent();
        intent.setParam("uri",uri);
        Operation operation = new Intent.OperationBuilder()
                .withBundleName("com.yuyh.imgsel")
                .withAbilityName(BasicAbility.class.getName())
                .build();
        intent.setOperation(operation);
        return intent;
    }

    private static final String KEY_FRAME_RECT_LEFT = "FrameRectLeft";
    private static final String KEY_FRAME_RECT_TOP = "FrameRectTop";
    private static final String KEY_FRAME_RECT_RIGHT = "FrameRectRight";
    private static final String KEY_FRAME_RECT_BOTTOM = "FrameRectBottom";
    private static final String KEY_SOURCE_URI = "SourceUri";

    // Views ///////////////////////////////////////////////////////////////////////////////////////
    public CropImageView mCropView;
    private String format = "jpeg";
    private RectFloat mFrameRect = null;
    private Uri mSourceUri = null;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_basic);
        mCropView = (CropImageView) findComponentById(ResourceTable.Id_cropImageView);
        bindViews();
        if (intent.getFloatParam(KEY_FRAME_RECT_LEFT,0) != 0) {
            // restore data
            mFrameRect = new RectFloat(intent.getFloatParam(KEY_FRAME_RECT_LEFT,0),
                    intent.getFloatParam(KEY_FRAME_RECT_TOP,0),
                    intent.getFloatParam(KEY_FRAME_RECT_RIGHT,0),
                    intent.getFloatParam(KEY_FRAME_RECT_BOTTOM,0));
            mSourceUri = intent.getSequenceableParam(KEY_SOURCE_URI);
        }
        if (mSourceUri == null) {
            // default data
            mSourceUri = intent.getSequenceableParam("uri");
            System.out.println("aoki  mSourceUri = "+mSourceUri);
        }
        mCropView.load(mSourceUri)
                .initialFrameRect(mFrameRect)
                .useThumbnail(true)
                .execute(mLoadCallback);
    }

    @Override
    protected void onForeground(Intent intent) {
        super.onForeground(intent);
        intent.setParam(KEY_FRAME_RECT_LEFT,mCropView.getActualCropRect().left);
        intent.setParam(KEY_FRAME_RECT_TOP,mCropView.getActualCropRect().top);
        intent.setParam(KEY_FRAME_RECT_RIGHT,mCropView.getActualCropRect().right);
        intent.setParam(KEY_FRAME_RECT_BOTTOM,mCropView.getActualCropRect().bottom);
        intent.setParam(KEY_SOURCE_URI, mCropView.getSourceUri());
    }

    private void bindViews() {
        mCropView = (CropImageView) findComponentById(ResourceTable.Id_cropImageView);
        findComponentById(ResourceTable.Id_buttonDone).setClickedListener(btnListener);
        findComponentById(ResourceTable.Id_buttonPickImage).setClickedListener(btnListener);
    }

    // Callbacks ///////////////////////////////////////////////////////////////////////////////////

    private final LoadCallback mLoadCallback = new LoadCallback() {
        @Override public void onSuccess() {
        }

        @Override public void onError(Throwable e) {
        }
    };

    private final Component.ClickedListener btnListener = new Component.ClickedListener() {
        @Override public void onClick(Component v) {
            if (v.getId()==ResourceTable.Id_buttonDone){
                if (verifySelfPermission("ohos.permission.WRITE_USER_STORAGE") != IBundleManager.PERMISSION_GRANTED) {
                    if (canRequestPermission("ohos.permission.WRITE_USER_STORAGE")) {
                        requestPermissionsFromUser(
                                new String[] { "ohos.permission.WRITE_USER_STORAGE" } , 100);
                    } else {
                        // 显示应用需要权限的理由，提示用户进入设置授权
                    }
                } else {
                    mCropView.crop(mSourceUri).execute(mCropCallback);
                }
            }else if (v.getId()==ResourceTable.Id_buttonPickImage){
                terminateAbility();
            }
        }

    };

    @Override
    public void onRequestPermissionsFromUserResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsFromUserResult(requestCode, permissions, grantResults);
        if (requestCode == 100){
            mCropView.crop(mSourceUri).execute(mCropCallback);
        }
    }

    @Override
    protected void onAbilityResult(int requestCode, int resultCode, Intent resultData) {
        super.onAbilityResult(requestCode, resultCode, resultData);
        if(requestCode == 1 && resultCode == 1){
            mSourceUri = resultData.getSequenceableParam("uri");
            mCropView.load(mSourceUri)
                    .initialFrameRect(mFrameRect)
                    .useThumbnail(true)
                    .execute(mLoadCallback);
        }
    }

    private final CropCallback mCropCallback = new CropCallback() {
        @Override
        public void onSuccess(PixelMap cropped) {
            mCropView.save(cropped)
                    .compressFormat(format)
                    .execute(mSaveCallback);
        }

        @Override
        public void onError(Throwable e) {
        }
    };

    private final SaveCallback mSaveCallback = new SaveCallback() {
        @Override public void onSuccess(Uri outputUri) {
            startResultActivity(outputUri);
        }

        @Override public void onError(Throwable e) {

        }
    };

    public void startResultActivity(Uri uri) {
        if (isTerminating()) return;
        startAbility(ResultAbility.createIntent( uri));
    }

}
