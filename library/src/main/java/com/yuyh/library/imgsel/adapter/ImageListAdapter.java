package com.yuyh.library.imgsel.adapter;


import com.yuyh.library.imgsel.config.ISCameraConfig;
import com.yuyh.library.imgsel.config.ISListConfig;
import com.yuyh.library.imgsel.ResourceTable;
import com.yuyh.library.imgsel.bean.MyImage;
import com.yuyh.library.imgsel.single.BasicAbility;
import com.yuyh.library.imgsel.ui.ImgSelFragment;
import com.yuyh.library.imgsel.utils.FileUtils;
import com.yuyh.library.imgsel.utils.DisplayUtils;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.*;
import ohos.agp.components.element.StateElement;
import ohos.agp.window.dialog.ToastDialog;
import ohos.bundle.IBundleManager;
import ohos.security.SystemPermission;

import java.util.ArrayList;


public class ImageListAdapter extends BaseItemProvider {


    private static final int ITEM_TYPE_CAMERA = 0;  //第一个条目是相机
    private static final int ITEM_TYPE_NORMAL = 1;  //第一个条目不是相机
    private ISListConfig ISListConfig;
    private Ability ability;
    private ArrayList<MyImage> images;       //当前需要显示的所有的图片数据
    private ArrayList<MyImage> mSelectedImages; //全局保存的已经选中的图片数据
    private boolean isShowCamera;         //是否显示拍照按钮
    private int mImageSize;               //每个条目的大小
    private LayoutScatter mInflater;
    private OnImageItemClickListener listener;   //图片被点击的监听

    public void setOnImageItemClickListener(OnImageItemClickListener listener) {
        this.listener = listener;
    }

    public interface OnImageItemClickListener {
        void onImageItemClick(Component view, MyImage myImage, int position);
    }

    public void refreshData(ArrayList<MyImage> images) {
        if (images == null || images.size() == 0) this.images = new ArrayList<>();
        else this.images = images;
        notifyDataChanged();
    }

    /**
     * 构造方法
     */
    public ImageListAdapter(Ability ability, ArrayList<MyImage> images) {
        this.ability = ability;
        if (images == null || images.size() == 0) this.images = new ArrayList<>();
        else this.images = images;

        mImageSize = DisplayUtils.getImageItemWidth(this.ability);
        ISListConfig = ISListConfig.getInstance();
        isShowCamera = ISListConfig.isShowCamera();
        mSelectedImages = ISListConfig.getSelectedImages();
        mInflater = LayoutScatter.getInstance(ability);

    }


    public ViewHolder onCreateViewHolder(ComponentContainer parent, int viewType) {
        if (viewType == ITEM_TYPE_CAMERA) {
            return new CameraViewHolder(mInflater.parse(ResourceTable.Layout_adapter_camera_item, parent, false));
        }
        return new ImageViewHolder(mInflater.parse(ResourceTable.Layout_adapter_image_list_item, parent, false));
    }

    public void onBindViewHolder(ViewHolder holder, int position) {
        if (holder instanceof CameraViewHolder) {
            ((CameraViewHolder) holder).bindCamera();
        } else if (holder instanceof ImageViewHolder) {
            ((ImageViewHolder) holder).bind(position);
        }

    }

    @Override
    public int getItemComponentType(int position) {
        if (isShowCamera) return position == 0 ? ITEM_TYPE_CAMERA : ITEM_TYPE_NORMAL;
        return ITEM_TYPE_NORMAL;
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
        int type = getItemComponentType(i);
        ViewHolder viewHolder = onCreateViewHolder(componentContainer, type);
        onBindViewHolder(viewHolder, i);

        return viewHolder.getRootView();
    }


    @Override
    public int getCount() {
        return isShowCamera ? images.size() + 1 : images.size();
    }

    public MyImage getItem(int position) {
        if (isShowCamera) {
            if (position == 0) return null;
            return images.get(position - 1);
        } else {
            return images.get(position);
        }
    }

    private class ImageViewHolder extends ViewHolder {

        Component rootView;
        Image ivThumb;
        Component mask;
        Component checkView;
        Checkbox cbCheck;


        ImageViewHolder(Component itemView) {
            rootView = itemView;
            ivThumb = (Image) itemView.findComponentById(ResourceTable.Id_iv_thumb);
            mask = itemView.findComponentById(ResourceTable.Id_mask);
            checkView = itemView.findComponentById(ResourceTable.Id_checkView);
            cbCheck = (Checkbox) itemView.findComponentById(ResourceTable.Id_cb_check);
            itemView.setLayoutConfig(new ListContainer.LayoutConfig(mImageSize, mImageSize)); //让图片是个正方形
        }

        void bind(final int position) {
            final MyImage myImage = getItem(position);
            ivThumb.setClickedListener(new Component.ClickedListener() {
                @Override
                public void onClick(Component v) {
                    if (ISListConfig.isMultiMode()) {
                        if (listener != null) listener.onImageItemClick(rootView, myImage, position);
                    }else {
                        ability.startAbility(BasicAbility.createIntent(images.get(position).getUri()));
                    }
                }
            });
            checkView.setClickedListener(new Component.ClickedListener() {
                @Override
                public void onClick(Component v) {
                    cbCheck.setChecked(!cbCheck.isChecked());
                    int selectLimit = ISListConfig.getSelectLimit();
                    if (cbCheck.isChecked() && mSelectedImages.size() >= selectLimit) {
                        cbCheck.setChecked(false);
                        new ToastDialog(ability.getContext()).setText("最多选择"+selectLimit+"张图片").setDuration(2000).show();
                        mask.setVisibility(Component.HIDE);
                    } else {
                        ISListConfig.addSelectedImageItem(position, myImage, cbCheck.isChecked());
                        mask.setVisibility(Component.VISIBLE);
                    }
                }
            });
            StateElement stateElement = new StateElement();
            stateElement.addState(new int[]{ComponentState.COMPONENT_STATE_CHECKED}, FileUtils.getPixelMapDrawable(ability, ResourceTable.Media_ic_checked));
            stateElement.addState(new int[]{ComponentState.COMPONENT_STATE_EMPTY}, FileUtils.getPixelMapDrawable(ability, ResourceTable.Media_ic_uncheck));
            cbCheck.setButtonElement(stateElement);
            //根据是否多选，显示或隐藏checkbox
            if (ISListConfig.isMultiMode()) {
                cbCheck.setVisibility(Component.VISIBLE);
                boolean checked = mSelectedImages.contains(myImage);
                if (checked) {
                    mask.setVisibility(Component.VISIBLE);
                    cbCheck.setChecked(true);
                } else {
                    mask.setVisibility(Component.HIDE);
                    cbCheck.setChecked(false);
                }
            } else {
                cbCheck.setVisibility(Component.HIDE);
            }
            ISListConfig.getImageLoader().displayImage(ability, myImage.uriSchema, ivThumb, mImageSize, mImageSize); //显示图片
        }

        @Override
        public Component getRootView() {
            return rootView;
        }
    }

    private class CameraViewHolder extends ViewHolder {

        Component mItemView;

        CameraViewHolder(Component itemView) {
            mItemView = itemView;
        }

        void bindCamera() {
            mItemView.setLayoutConfig(new ListContainer.LayoutConfig(mImageSize, mImageSize)); //让图片是个正方形
            mItemView.setTag(null);
            mItemView.setClickedListener(new Component.ClickedListener() {
                @Override
                public void onClick(Component v) {
                    if (ability.verifySelfPermission(SystemPermission.CAMERA) != IBundleManager.PERMISSION_GRANTED) {
                        ability.requestPermissionsFromUser(new String[]{SystemPermission.CAMERA}, ImgSelFragment.REQUEST_PERMISSION_CAMERA);
                    } else {
                        Intent intent=new Intent();
                        Operation operation = new Intent.OperationBuilder()
                                .withBundleName("com.yuyh.imgsel")
                                .withAbilityName(ISCameraConfig.class.getName())
                                .build();
                        intent.setOperation(operation);
                        ability.startAbility(intent);
                    }
                }
            });
        }

        @Override
        public Component getRootView() {
            return mItemView;
        }
    }

    public abstract class ViewHolder {
        public abstract Component getRootView();
    }
}
