package com.yuyh.library.imgsel;


import com.yuyh.library.imgsel.bean.Folder;
import com.yuyh.library.imgsel.bean.MyImage;
import com.yuyh.library.imgsel.config.ISListConfig;
import com.yuyh.library.imgsel.utils.FileUtils;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.DataAbilityHelper;
import ohos.aafwk.ability.DataAbilityRemoteException;
import ohos.data.dataability.DataAbilityPredicates;
import ohos.data.resultset.ResultSet;
import ohos.media.photokit.metadata.AVStorage;
import ohos.utils.net.Uri;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ISNav {

    public static final int LOADER_ALL = 0;         //加载所有图片
    public static final int LOADER_CATEGORY = 1;    //分类加载图片
    private final String[] IMAGE_PROJECTION = {     //查询图片需要的数据列
            AVStorage.Images.Media.DISPLAY_NAME,   //图片的显示名称  aaa.jpg
            AVStorage.Images.Media.DATA,           //图片的真实路径  /storage/emulated/0/pp/downloader/wallpaper/aaa.jpg
            AVStorage.Images.Media.SIZE,           //图片的大小，long型  132492
            AVStorage.Images.Media.MIME_TYPE,      //图片的类型     image/jpeg
            AVStorage.Images.Media.DATE_ADDED,
            AVStorage.Images.Media.ID
    };    //图片被添加的时间，long型  1450518608

    private Ability ability;
    private OnImagesLoadedListener loadedListener;                     //图片加载完成的回调接口
    private ArrayList<Folder> folders = new ArrayList<>();   //所有的图片文件夹
    private DataAbilityHelper helper;
    private DataAbilityPredicates dataAbilityPredicates;
    private ResultSet resultSet;


    public ISNav(Ability ability, String path, OnImagesLoadedListener loadedListener) {
        this.ability = ability;
        this.loadedListener = loadedListener;

        helper = DataAbilityHelper.creator(ability);
        dataAbilityPredicates = new DataAbilityPredicates();

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    if (path == null) {
                        resultSet = helper.query(AVStorage.Images.Media.EXTERNAL_DATA_ABILITY_URI, null, null);

                    } else {
                        resultSet = helper.query(Uri.getUriFromFile(new File(path)), null, null);
                    }
                    ability.getUITaskDispatcher().syncDispatch(new Runnable() {
                        @Override
                        public void run() {
                            onLoadFinished(resultSet);

                        }
                    });
                } catch (DataAbilityRemoteException e) {
                    onLoadFinished(resultSet);
                }
            }
        }).start();

    }



    public void onLoadFinished(ResultSet data) {
        folders.clear();
        if (data != null) {
            ArrayList<MyImage> allImages = new ArrayList<>();   //所有图片的集合,不分文件夹
            while (data.goToNextRow()) {
                String[] all = data.getAllColumnNames();
                List<String> a = Arrays.asList(all);

                //查询数据图片
                String imageName = data.getString(data.getColumnIndexForName(IMAGE_PROJECTION[0]));
                String imagePath = data.getString(data.getColumnIndexForName(IMAGE_PROJECTION[1]));
                String id = data.getString(data.getColumnIndexForName(AVStorage.Images.Media.ID));
                Uri uri = Uri.appendEncodedPathToUri(AVStorage.Images.Media.EXTERNAL_DATA_ABILITY_URI, String.valueOf(id));
                File file = new File(imagePath);
                if (!file.exists() || file.length() <= 0) {
                    continue;
                }

                long imageSize = data.getLong(data.getColumnIndexForName(IMAGE_PROJECTION[2]));
                int imageWidth = data.getInt(data.getColumnIndexForName("width"));
                int imageHeight = data.getInt(data.getColumnIndexForName("height"));
                String imageMimeType = data.getString(data.getColumnIndexForName(IMAGE_PROJECTION[3]));
                long imageAddTime = data.getLong(data.getColumnIndexForName(IMAGE_PROJECTION[4]));
                //封装实体
                MyImage myImage = new MyImage();
                myImage.name = imageName;
                myImage.uriSchema = uri.toString();
                myImage.size = imageSize;
                myImage.width = imageWidth;
                myImage.height = imageHeight;
                myImage.mimeType = imageMimeType;
                myImage.addTime = imageAddTime;
                myImage.uri=uri;
                allImages.add(myImage);


                //根据父路径分类存放图片
                File imageFile = new File(imagePath);
                File imageParentFile = imageFile.getParentFile();
                Folder folder = new Folder();
                folder.name = imageParentFile.getName();
                folder.path = imageParentFile.getAbsolutePath();

                if (!folders.contains(folder)) {
                    ArrayList<MyImage> images = new ArrayList<>();
                    images.add(myImage);
                    folder.cover = myImage;
                    folder.images = images;
                    folders.add(folder);
                } else {
                    folders.get(folders.indexOf(folder)).images.add(myImage);
                }
            }
            //防止没有图片报异常
            if (data.getColumnCount() > 0 && allImages.size() > 0) {
                //构造所有图片的集合
                Folder allImagesFolder = new Folder();
                allImagesFolder.name = FileUtils.getString(ability, ResourceTable.String_ip_all_images);
                allImagesFolder.path = "/";
                allImagesFolder.cover = allImages.get(0);
                allImagesFolder.images = allImages;
                folders.add(0, allImagesFolder);  //确保第一条是所有图片
            }
        }

        //回调接口，通知图片数据准备完成
        ISListConfig.getInstance().setImageFolders(folders);
        loadedListener.onImagesLoaded(folders);
    }

    public interface OnImagesLoadedListener {
        void onImagesLoaded(List<Folder> folders);
    }
}
