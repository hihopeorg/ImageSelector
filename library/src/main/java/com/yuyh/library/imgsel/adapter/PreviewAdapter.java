package com.yuyh.library.imgsel.adapter;


import com.github.chrisbanes.photoview.PhotoView;
import com.yuyh.library.imgsel.config.ISListConfig;
import com.yuyh.library.imgsel.bean.MyImage;
import ohos.aafwk.ability.Ability;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.PageSliderProvider;
import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayManager;

import java.util.ArrayList;

public class PreviewAdapter extends PageSliderProvider {

    private int screenWidth;
    private int screenHeight;
    private ISListConfig ISListConfig;
    private ArrayList<MyImage> images = new ArrayList<>();
    private Ability ability;
    public PhotoViewClickListener listener;

    public PreviewAdapter(Ability ability, ArrayList<MyImage> images) {
        this.ability = ability;
        this.images = images;

        DisplayManager displayManager = DisplayManager.getInstance();
        Display display = displayManager.getDefaultDisplay(ability).get();

//        DisplayMetrics dm = Utils.getScreenPix(ability);
        screenWidth = display.getAttributes().width;
        screenHeight = display.getAttributes().height;
        ISListConfig = ISListConfig.getInstance();
    }

    public void setData(ArrayList<MyImage> images) {
        this.images = images;
    }

    public void setPhotoViewClickListener(PhotoViewClickListener listener) {
        this.listener = listener;
    }


    @Override
    public int getCount() {
        return images.size();
    }

    @Override
    public Object createPageInContainer(ComponentContainer componentContainer, int position) {
//        Image photoView = new Image(mActivity);
        PhotoView photoView = new PhotoView(ability);
        MyImage myImage = images.get(position);
        photoView.setLayoutConfig(new ComponentContainer.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT, ComponentContainer.LayoutConfig.MATCH_PARENT));

        ISListConfig.getImageLoader().displayImagePreview(ability, myImage.uriSchema, photoView, screenWidth, screenHeight);

        photoView.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (listener != null) listener.OnPhotoTapListener(component, 0, 0);

            }
        });
        componentContainer.addComponent(photoView);
        return photoView;
    }

    @Override
    public void destroyPageFromContainer(ComponentContainer componentContainer, int i, Object o) {
        componentContainer.removeComponent((Component) o);
    }

    @Override
    public boolean isPageMatchToObject(Component component, Object o) {
        return component == o;
    }

    @Override
    public int getPageIndex(Object object) {
        return super.getPageIndex(object);
    }


    public interface PhotoViewClickListener {
        void OnPhotoTapListener(Component view, float v, float v1);
    }
}
