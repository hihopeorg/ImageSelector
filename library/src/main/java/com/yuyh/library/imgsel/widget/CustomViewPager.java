package com.yuyh.library.imgsel.widget;


import com.yuyh.library.imgsel.ResourceTable;
import com.yuyh.library.imgsel.bean.MyImage;
import com.yuyh.library.imgsel.config.ISListConfig;
import com.yuyh.library.imgsel.utils.FileUtils;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.agp.components.element.StateElement;
import ohos.agp.window.dialog.ToastDialog;


public class CustomViewPager extends DividerGridItemDecoration implements ISListConfig.OnImageSelectedListener, Component.ClickedListener, AbsButton.CheckedStateChangedListener {

    private Checkbox mCbCheck;                //是否选中当前图片的Checkbox
    private Button mBtnOk;                         //确认图片的选择
    private Component bottomBar;
    private Component marginView;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);

        ISListConfig.addOnImageSelectedListener(this);
        mBtnOk = (Button) findComponentById(ResourceTable.Id_btn_ok);
        mBtnOk.setVisibility(Component.VISIBLE);
        mBtnOk.setClickedListener(this);


        bottomBar = findComponentById(ResourceTable.Id_bottom_bar);
        bottomBar.setVisibility(Component.HIDE);

        mCbCheck = (Checkbox) findComponentById(ResourceTable.Id_cb_check);

        marginView = findComponentById(ResourceTable.Id_margin_bottom);
        StateElement stateElement = new StateElement();
        stateElement.addState(new int[]{ComponentState.COMPONENT_STATE_CHECKED}, FileUtils.getPixelMapDrawable(this, ResourceTable.Media_ic_checked));
        stateElement.addState(new int[]{ComponentState.COMPONENT_STATE_EMPTY}, FileUtils.getPixelMapDrawable(this, ResourceTable.Media_ic_uncheck));
        mCbCheck.setButtonElement(stateElement);
        //初始化当前页面的状态
        onImageSelected(0, null, false);
        MyImage item = mMyImages.get(mCurrentPosition);
        boolean isSelected = ISListConfig.isSelect(item);
        mTitleCount.setText(FileUtils.getString(this, ResourceTable.String_ip_preview_image_count, mCurrentPosition + 1, mMyImages.size()));
        mCbCheck.setChecked(isSelected);
        //滑动ViewPager的时候，根据外界的数据改变当前的选中状态和当前的图片的位置描述文本
        mViewPager.addPageChangedListener(new PageSlider.PageChangedListener() {
            @Override
            public void onPageSliding(int i, float v, int i1) {

            }

            @Override
            public void onPageSlideStateChanged(int i) {

            }

            @Override
            public void onPageChosen(int position) {

                mCurrentPosition = position;
                MyImage item = mMyImages.get(mCurrentPosition);
                boolean isSelected = ISListConfig.isSelect(item);

                mCbCheck.setChecked(isSelected);
                mTitleCount.setText(FileUtils.getString(getContext(), ResourceTable.String_ip_preview_image_count, mCurrentPosition + 1, mMyImages.size()));

            }

        });
        //当点击当前选中按钮的时候，需要根据当前的选中状态添加和移除图片
        mCbCheck.setCheckedStateChangedListener(new AbsButton.CheckedStateChangedListener() {
            @Override
            public void onCheckedChanged(AbsButton absButton, boolean b) {

                int selectLimit = ISListConfig.getSelectLimit();
                if(selectedImages.size() >= selectLimit){
                    if(b){
                        new ToastDialog(getContext()).setText("最多选择"+selectLimit+"张图片").setDuration(2000).show();
                        mCbCheck.setChecked(false);
                    }else {
                        ISListConfig.addSelectedImageItem(mCurrentPosition, mMyImages.get(mCurrentPosition), mCbCheck.isChecked());
                    }
                }else{
                        ISListConfig.addSelectedImageItem(mCurrentPosition, mMyImages.get(mCurrentPosition), mCbCheck.isChecked());
                }
            }

        });


    }



    @Override
    public void onImageSelected(int position, MyImage item, boolean isAdd) {
        if (ISListConfig.getSelectImageCount() > 0) {
            mBtnOk.setText(FileUtils.getString(this, ResourceTable.String_ip_select_complete, ISListConfig.getSelectImageCount(), ISListConfig.getSelectLimit()));

        } else {
            mBtnOk.setText(FileUtils.getString(this, ResourceTable.String_ip_complete));
        }


    }

    @Override
    public void onClick(Component v) {
        int id = v.getId();
        if (id == ResourceTable.Id_btn_ok) {
            if (ISListConfig.getSelectedImages().size() == 0) {
                mCbCheck.setChecked(true);
                MyImage myImage = mMyImages.get(mCurrentPosition);
                ISListConfig.addSelectedImageItem(mCurrentPosition, myImage, mCbCheck.isChecked());
            }
            Intent intent = new Intent();
            intent.setParam(ISListConfig.EXTRA_RESULT_ITEMS, ISListConfig.getSelectedImages());
            setResult(ISListConfig.RESULT_CODE_ITEMS, intent);
            terminateAbility();

        } else if (id == ResourceTable.Id_btn_back) {
            Intent intent = new Intent();

            intent.setParam(ISListConfig.EXTRA_RESULT_ITEMS, ISListConfig.getSelectedImages());
            setResult(ISListConfig.RESULT_CODE_BACK, intent);
            terminateAbility();
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();

        setResult(ISListConfig.RESULT_CODE_BACK, intent);
        terminateAbility();
        super.onBackPressed();
    }

    @Override
    protected void onStop() {
        ISListConfig.removeOnImageSelectedListener(this);
        super.onStop();
    }



    @Override
    public void onCheckedChanged(AbsButton absButton, boolean b) {
        int id = absButton.getId();
        if (id == ResourceTable.Id_cb_origin) {
            if (b) {
                long size = 0;
                for (MyImage item : selectedImages)
                    size += item.size;

            } else {

            }
        }
    }
}
