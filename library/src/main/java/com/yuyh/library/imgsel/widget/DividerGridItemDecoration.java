package com.yuyh.library.imgsel.widget;


import com.yuyh.library.imgsel.config.ISListConfig;
import com.yuyh.library.imgsel.ResourceTable;
import com.yuyh.library.imgsel.adapter.PreviewAdapter;
import com.yuyh.library.imgsel.bean.MyImage;
import com.yuyh.library.imgsel.utils.FileUtils;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.PageSlider;
import ohos.agp.components.Text;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public abstract class DividerGridItemDecoration extends com.yuyh.library.imgsel.ui.ISListActivity {

    protected ISListConfig ISListConfig;
    protected ArrayList<MyImage> mMyImages;      //跳转进ImagePreviewFragment的图片文件夹
    protected int mCurrentPosition = 0;              //跳转进ImagePreviewFragment时的序号，第几个图片
    protected Text mTitleCount;                  //显示当前图片的位置  例如  5/31
    protected ArrayList<MyImage> selectedImages;   //所有已经选中的图片
    protected Component content;
    protected Component topBar;
    protected PageSlider mViewPager;
    protected PreviewAdapter pro;
    protected boolean isFromItems = false;


    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_image_preview);

        mCurrentPosition = intent.getIntParam(ISListConfig.EXTRA_SELECTED_IMAGE_POSITION, 0);
        isFromItems = intent.getBooleanParam(ISListConfig.EXTRA_FROM_ITEMS, false);

        if (isFromItems) {
            mMyImages = (ArrayList<MyImage>) getIntent().getSerializableParam(ISListConfig.EXTRA_IMAGE_ITEMS);
        } else {
            mMyImages = (ArrayList<MyImage>) DataHolder.getInstance().retrieve(DataHolder.DH_CURRENT_IMAGE_FOLDER_ITEMS);
        }

        ISListConfig = ISListConfig.getInstance();
        selectedImages = ISListConfig.getSelectedImages();

        //初始化控件
        content = findComponentById(ResourceTable.Id_content);

        //因为状态栏透明后，布局整体会上移，所以给头部加上状态栏的margin值，保证头部不会被覆盖
        topBar = findComponentById(ResourceTable.Id_top_bar);

        topBar.findComponentById(ResourceTable.Id_btn_ok).setVisibility(Component.HIDE);
        topBar.findComponentById(ResourceTable.Id_btn_back).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component v) {
               terminateAbility();
            }
        });

        mTitleCount = (Text) findComponentById(ResourceTable.Id_tv_des);

        mViewPager = (PageSlider) findComponentById(ResourceTable.Id_viewpager);
        pro = new PreviewAdapter(this, mMyImages);
        pro.setPhotoViewClickListener(new PreviewAdapter.PhotoViewClickListener() {
            @Override
            public void OnPhotoTapListener(Component view, float v, float v1) {
            }
        });
        mViewPager.setProvider(pro);
        mViewPager.setCurrentPage(mCurrentPosition, false);

        //初始化当前页面的状态
        mTitleCount.setText(FileUtils.getString(this,ResourceTable.String_ip_preview_image_count, mCurrentPosition + 1, mMyImages.size()));
    }
    public static class DataHolder {
        public static final String DH_CURRENT_IMAGE_FOLDER_ITEMS = "dh_current_image_folder_items";

        private static DataHolder mInstance;
        private Map<String, List<MyImage>> data;

        public static DataHolder getInstance() {
            if (mInstance == null){
                synchronized (DataHolder.class){
                    if (mInstance == null){
                        mInstance = new DataHolder();
                    }
                }
            }
            return mInstance;
        }

        private DataHolder() {
            data = new HashMap<>();
        }

        public void save(String id, List<MyImage> object) {
            if (data != null){
                data.put(id, object);
            }
        }

        public Object retrieve(String id) {
            if (data == null || mInstance == null){
                throw new RuntimeException("未初始化");
            }
            return data.get(id);
        }
    }

}