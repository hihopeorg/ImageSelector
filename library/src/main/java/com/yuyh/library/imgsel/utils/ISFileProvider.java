package com.yuyh.library.imgsel.utils;

import com.nostra13.universalimageloader.core.assist.ImageSize;
import com.yuyh.library.imgsel.common.ImageLoader;
import ohos.aafwk.ability.Ability;
import ohos.agp.components.Image;


public class ISFileProvider implements ImageLoader {

    @Override
    public void displayImage(Ability ability, String uriScheme, Image imageView, int width, int height) {

        ImageSize size = new ImageSize(width, height);
        com.nostra13.universalimageloader.core.ImageLoader.getInstance().displayImage(uriScheme, imageView, size);
    }

    @Override
    public void displayImagePreview(Ability ability, String uriScheme, Image imageView, int width, int height) {
        ImageSize size = new ImageSize(width, height);
        com.nostra13.universalimageloader.core.ImageLoader.getInstance().displayImage(uriScheme, imageView, size);
    }

}
