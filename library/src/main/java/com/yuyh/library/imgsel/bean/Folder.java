package com.yuyh.library.imgsel.bean;

import java.io.Serializable;
import java.util.ArrayList;


public class Folder implements Serializable {

    public String name;  //当前文件夹的名字
    public String path;  //当前文件夹的路径
    public MyImage cover;   //当前文件夹需要要显示的缩略图，默认为最近的一次图片
    public ArrayList<MyImage> images;  //当前文件夹下所有图片的集合


    @Override
    public boolean equals(Object o) {
        try {
            Folder other = (Folder) o;
            return this.path.equalsIgnoreCase(other.path);
        } catch (ClassCastException e) {
            return super.equals(o);
        }

    }
}
