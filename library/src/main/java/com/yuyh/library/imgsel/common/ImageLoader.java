package com.yuyh.library.imgsel.common;

import ohos.aafwk.ability.Ability;
import ohos.agp.components.Image;

import java.io.Serializable;


public interface ImageLoader extends Serializable {

    void displayImage(Ability ability,String uriScheme, Image imageView, int width, int height);

    void displayImagePreview(Ability ability, String uriScheme, Image imageView, int width, int height);

}
