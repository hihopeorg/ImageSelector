/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Issei Aoki
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.yuyh.library.imgsel.single;

public interface CropImageViewAttr {
    String scv_crop_mode = "scv_crop_mode";
    String scv_background_color = "scv_background_color";
    String scv_overlay_color = "scv_overlay_color";
    String scv_frame_color = "scv_frame_color";
    String scv_handle_color = "scv_handle_color";
    String scv_guide_color = "scv_guide_color";
    String scv_guide_show_mode = "scv_guide_show_mode";
    String scv_handle_show_mode = "scv_handle_show_mode";
    String scv_handle_size = "scv_handle_size";
    String scv_touch_padding = "scv_touch_padding";
    String scv_min_frame_size = "scv_min_frame_size";
    String scv_frame_stroke_weight = "scv_frame_stroke_weight";
    String scv_guide_stroke_weight = "scv_guide_stroke_weight";
    String scv_crop_enabled = "scv_crop_enabled";
    String scv_initial_frame_scale = "scv_initial_frame_scale";
    String scv_animation_enabled = "scv_animation_enabled";
    String scv_animation_duration = "scv_animation_duration";
    String scv_handle_shadow_enabled = "scv_handle_shadow_enabled";
}
