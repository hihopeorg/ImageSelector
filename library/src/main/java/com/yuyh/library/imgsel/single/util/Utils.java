/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Issei Aoki
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.yuyh.library.imgsel.single.util;

import ohos.aafwk.ability.DataAbilityHelper;
import ohos.aafwk.ability.DataAbilityRemoteException;
import ohos.aafwk.content.Intent;
import ohos.agp.render.opengl.GLES1X;
import ohos.agp.utils.Matrix;
import ohos.app.Context;
import ohos.data.rdb.ValuesBucket;
import ohos.data.resultset.ResultSet;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.media.image.common.Rect;
import ohos.media.photokit.metadata.AVStorage;
import ohos.utils.net.Uri;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("unused")
public class Utils {
    private static final String TAG = Utils.class.getSimpleName();
    private static final int SIZE_DEFAULT = 2048;
    private static final int SIZE_LIMIT = 4096;
    public static int sInputImageWidth = 0;
    public static int sInputImageHeight = 0;

    /**
     * Copy EXIF info to new file
     * <p>
     * =========================================
     * <p>
     * NOTE: PNG cannot not have EXIF info.
     * <p>
     * source: JPEG, save: JPEG
     * copies all EXIF data
     * <p>
     * source: JPEG, save: PNG
     * saves no EXIF data
     * <p>
     * source: PNG, save: JPEG
     * saves only width and height EXIF data
     * <p>
     * source: PNG, save: PNG
     * saves no EXIF data
     * <p>
     * =========================================
     */
    public static void copyExifInfo(Context context, Uri sourceUri, Uri saveUri, int outputWidth, int outputHeight) {
        if (sourceUri == null || saveUri == null) return;
        try {
            File sourceFile = Utils.getFileFromUri(context, sourceUri);
            File saveFile = Utils.getFileFromUri(context, saveUri);
            if (sourceFile == null || saveFile == null) {
                return;
            }
            String sourcePath = sourceFile.getAbsolutePath();
            String savePath = saveFile.getAbsolutePath();

            ExifInterface sourceExif = new ExifInterface(sourcePath);
            List<String> tags = new ArrayList<>();
            tags.add(ExifInterface.TAG_DATETIME);
            tags.add(ExifInterface.TAG_FLASH);
            tags.add(ExifInterface.TAG_FOCAL_LENGTH);
            tags.add(ExifInterface.TAG_GPS_ALTITUDE);
            tags.add(ExifInterface.TAG_GPS_ALTITUDE_REF);
            tags.add(ExifInterface.TAG_GPS_DATESTAMP);
            tags.add(ExifInterface.TAG_GPS_LATITUDE);
            tags.add(ExifInterface.TAG_GPS_LATITUDE_REF);
            tags.add(ExifInterface.TAG_GPS_LONGITUDE);
            tags.add(ExifInterface.TAG_GPS_LONGITUDE_REF);
            tags.add(ExifInterface.TAG_GPS_PROCESSING_METHOD);
            tags.add(ExifInterface.TAG_GPS_TIMESTAMP);
            tags.add(ExifInterface.TAG_MAKE);
            tags.add(ExifInterface.TAG_MODEL);
            tags.add(ExifInterface.TAG_WHITE_BALANCE);

            tags.add(ExifInterface.TAG_EXPOSURE_TIME);
            //noinspection deprecation
            tags.add(ExifInterface.TAG_APERTURE);
            //noinspection deprecation
            tags.add(ExifInterface.TAG_ISO);

            tags.add(ExifInterface.TAG_DATETIME_DIGITIZED);
            tags.add(ExifInterface.TAG_SUBSEC_TIME);
            //noinspection deprecation
            tags.add(ExifInterface.TAG_SUBSEC_TIME_DIG);
            //noinspection deprecation
            tags.add(ExifInterface.TAG_SUBSEC_TIME_ORIG);

            tags.add(ExifInterface.TAG_F_NUMBER);
            tags.add(ExifInterface.TAG_ISO_SPEED_RATINGS);
            tags.add(ExifInterface.TAG_SUBSEC_TIME_DIGITIZED);
            tags.add(ExifInterface.TAG_SUBSEC_TIME_ORIGINAL);

            ExifInterface saveExif = new ExifInterface(savePath);
            String value;
            for (String tag : tags) {
                value = sourceExif.getAttribute(tag);
                if (!TextUtils.isEmpty(value)) {
                    saveExif.setAttribute(tag, value);
                }
            }
            saveExif.setAttribute(ExifInterface.TAG_IMAGE_WIDTH, String.valueOf(outputWidth));
            saveExif.setAttribute(ExifInterface.TAG_IMAGE_LENGTH, String.valueOf(outputHeight));
            saveExif.setAttribute(ExifInterface.TAG_ORIENTATION,
                    String.valueOf(ExifInterface.ORIENTATION_UNDEFINED));

            saveExif.saveAttributes();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static int getExifRotation(File file) {
        if (file == null) return 0;
        try {
            ExifInterface exif = new ExifInterface(file.getAbsolutePath());
            return getRotateDegreeFromOrientation(
                    exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED));
        } catch (IOException e) {
        }
        return 0;
    }

    public static int getExifRotation(Context context, Uri uri) {
        ResultSet cursor = null;
        String[] projection = {AVStorage.Images.Media.ID};
        DataAbilityHelper helper = DataAbilityHelper.creator(context);
        try {
            cursor = helper.query(uri, projection, null);
            if (cursor == null || !cursor.goToFirstRow()) {
                return 0;
            }
            return cursor.getInt(0);
        } catch (RuntimeException | DataAbilityRemoteException ignored) {
            return 0;
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    public static int getExifOrientation(Context context, Uri uri) {
        String authority = uri.getDecodedAuthority().toLowerCase();
        int orientation;
        if (authority.endsWith("media")) {
            orientation = getExifRotation(context, uri);
        } else {
            orientation = getExifRotation(getFileFromUri(context, uri));
        }
        return orientation;
    }

    public static int getRotateDegreeFromOrientation(int orientation) {
        int degree = 0;
        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                degree = 90;
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                degree = 180;
                break;
            case ExifInterface.ORIENTATION_ROTATE_270:
                degree = 270;
                break;
            default:
                break;
        }
        return degree;
    }

    public static Matrix getMatrixFromExifOrientation(int orientation) {
        Matrix matrix = new Matrix();
        switch (orientation) {
            case ExifInterface.ORIENTATION_UNDEFINED:
                break;
            case ExifInterface.ORIENTATION_NORMAL:
                break;
            case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
                matrix.postScale(-1.0f, 1.0f);
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                matrix.postRotate(180.0f);
                break;
            case ExifInterface.ORIENTATION_FLIP_VERTICAL:
                matrix.postScale(1.0f, -1.0f);
                break;
            case ExifInterface.ORIENTATION_ROTATE_90:
                matrix.postRotate(90.0f);
                break;
            case ExifInterface.ORIENTATION_TRANSVERSE:
                matrix.postRotate(-90.0f);
                matrix.postScale(1.0f, -1.0f);
                break;
            case ExifInterface.ORIENTATION_TRANSPOSE:
                matrix.postRotate(90.0f);
                matrix.postScale(1.0f, -1.0f);
                break;
            case ExifInterface.ORIENTATION_ROTATE_270:
                matrix.postRotate(-90.0f);
                break;
        }
        return matrix;
    }

    public static int getExifOrientationFromAngle(int angle) {
        int normalizedAngle = angle % 360;
        switch (normalizedAngle) {
            case 0:
                return ExifInterface.ORIENTATION_NORMAL;
            case 90:
                return ExifInterface.ORIENTATION_ROTATE_90;
            case 180:
                return ExifInterface.ORIENTATION_ROTATE_180;
            case 270:
                return ExifInterface.ORIENTATION_ROTATE_270;
            default:
                return ExifInterface.ORIENTATION_NORMAL;
        }
    }

    @SuppressWarnings("ResourceType")
    public static Uri ensureUriPermission(Context context, Intent intent) {
        Uri uri = intent.getUri();
        return uri;
    }

    /**
     * Get image file from uri
     *
     * @param context The context
     * @param uri     The Uri of the image
     * @return Image file
     */
    public static File getFileFromUri(final Context context,
                                      final Uri uri) {
        String filePath = null;

         if ("content".equalsIgnoreCase(uri.getScheme())) {
            if (isGooglePhotosUri(uri)) {
                filePath = uri.getLastPath();
            } else {
                filePath = getDataColumn(context, uri, null, null);
            }
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            filePath = uri.getDecodedPath();
        }
        if (filePath != null) {
            return new File(filePath);
        }
        return null;
    }

    // A copy of com.ohos.providers.downloads.RawDocumentsHelper since it is invisibility.
    public static class RawDocumentsHelper {
        public static final String RAW_PREFIX = "raw:";

        public static boolean isRawDocId(String docId) {
            return docId != null && docId.startsWith(RAW_PREFIX);
        }

        public static String getDocIdForFile(File file) {
            return RAW_PREFIX + file.getAbsolutePath();
        }

        public static String getAbsoluteFilePath(String rawDocumentId) {
            return rawDocumentId.substring(RAW_PREFIX.length());
        }
    }

    /**
     * Get the value of the data column for this Uri. This is useful for
     * MediaStore Uris, and other file-based ContentProviders.
     *
     * @param context       The context.
     * @param uri           The Uri to query.
     * @param selection     (Optional) Filter used in the query.
     * @param selectionArgs (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     */
    public static String getDataColumn(Context context, Uri uri, String selection,
                                       String[] selectionArgs) {
        ResultSet resultSet = null;
        DataAbilityHelper helper = DataAbilityHelper.creator(context);
        try {
            resultSet = helper.query(AVStorage.Images.Media.EXTERNAL_DATA_ABILITY_URI, new String[]{AVStorage.Images.Media.DATA, AVStorage.Images.Media.VOLUME_NAME}, null);
            if (resultSet != null && resultSet.goToFirstRow()) {
                // 获取id字段的值
                String id = resultSet.getString(resultSet.getColumnIndexForName(AVStorage.Images.Media.DATA));
                return id;
            }
        } catch (DataAbilityRemoteException e) {
            e.printStackTrace();
        } finally {
            resultSet.close();
        }

        return null;
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.ohos.externalstorage.documents".equals(uri.getDecodedAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.ohos.providers.downloads.documents".equals(uri.getDecodedAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.ohos.providers.media.documents".equals(uri.getDecodedAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is Google Photos.
     */
    public static boolean isGooglePhotosUri(Uri uri) {
        return "com.google.ohos.apps.photos.content".equals(uri.getDecodedAuthority());
    }

    /**
     * @param uri The Uri to check
     * @return Whether the Uri authority is Google Drive.
     */
    public static boolean isGoogleDriveDocument(Uri uri) {
        return "com.google.ohos.apps.docs.storage".equals(uri.getDecodedAuthority());
    }


    public static PixelMap decodeSampledBitmapFromUri(Context context, Uri sourceUri, int requestSize) {
        InputStream stream = null;
        PixelMap bitmap = null;
        DataAbilityHelper helper = DataAbilityHelper.creator(context);
        try {
            stream = helper.obtainInputStream(sourceUri);
            if (stream != null) {
                ImageSource.DecodingOptions options = new ImageSource.DecodingOptions();
                options.sampleSize = Utils.calculateInSampleSize(context, sourceUri, requestSize);
                bitmap = ImageSource.create(stream, new ImageSource.SourceOptions()).createPixelmap(options);
            }
        } catch (FileNotFoundException | DataAbilityRemoteException e) {
        } finally {
            try {
                if (stream != null) {
                    stream.close();
                }
            } catch (IOException e) {
            }
        }
        return bitmap;
    }

    public static int calculateInSampleSize(Context context, Uri sourceUri, int requestSize) {
        DataAbilityHelper helper = DataAbilityHelper.creator(context);
        // check image size
        FileDescriptor fd = null;
        PixelMap pixelMap = null;
//        options.inJustDecodeBounds = true;
        try {
            fd = helper.openFile(sourceUri,"r");
            pixelMap = ImageSource.create(fd,null).createPixelmap(null);
        } catch (FileNotFoundException | DataAbilityRemoteException ignored) {
        }
        int inSampleSize = 1;
        sInputImageWidth = pixelMap.getImageInfo().size.width;
        sInputImageHeight = pixelMap.getImageInfo().size.height;
        while (pixelMap.getImageInfo().size.width / inSampleSize > requestSize
                || pixelMap.getImageInfo().size.height / inSampleSize > requestSize) {
            inSampleSize *= 2;
        }
        return inSampleSize;
    }

    public static PixelMap getScaledBitmapForHeight(PixelMap bitmap, int outHeight) {
        float currentWidth = bitmap.getImageInfo().size.width;
        float currentHeight = bitmap.getImageInfo().size.height;
        float ratio = currentWidth / currentHeight;
        int outWidth = Math.round(outHeight * ratio);
        return getScaledBitmap(bitmap, outWidth, outHeight);
    }

    public static PixelMap getScaledBitmapForWidth(PixelMap bitmap, int outWidth) {
        float currentWidth = bitmap.getImageInfo().size.width;
        float currentHeight = bitmap.getImageInfo().size.height;
        float ratio = currentWidth / currentHeight;
        int outHeight = Math.round(outWidth / ratio);
        return getScaledBitmap(bitmap, outWidth, outHeight);
    }

    public static PixelMap getScaledBitmap(PixelMap bitmap, int outWidth, int outHeight) {
        int currentWidth = bitmap.getImageInfo().size.width;
        int currentHeight = bitmap.getImageInfo().size.height;
        Matrix scaleMatrix = new Matrix();
        scaleMatrix.postScale((float) outWidth / (float) currentWidth,
                (float) outHeight / (float) currentHeight);
        return PixelMap.create(bitmap, new Rect(0, 0, currentWidth, currentHeight), null);
    }

    public static int getMaxSize() {
        int maxSize = SIZE_DEFAULT;
        int[] arr = new int[1];
        GLES1X.glGetIntegerv(GLES1X.GL_MAX_TEXTURE_SIZE, arr);
        if (arr[0] > 0) {
            maxSize = Math.min(arr[0], SIZE_LIMIT);
        }
        return maxSize;
    }

    public static void closeQuietly(Closeable closeable) {
        if (closeable == null) return;
        try {
            closeable.close();
        } catch (Throwable ignored) {
        }
    }

    public static void updateGalleryInfo(Context context, Uri uri) {
        DataAbilityHelper helper = DataAbilityHelper.creator(context);
        ValuesBucket values = new ValuesBucket();
        File file = getFileFromUri(context, uri);
        if (file != null && file.exists()) {
            values.putLong(AVStorage.Images.Media.SIZE, file.length());
        }
        try {
            helper.update(uri, values, null);
        } catch (DataAbilityRemoteException e) {
            e.printStackTrace();
        }
    }
}
