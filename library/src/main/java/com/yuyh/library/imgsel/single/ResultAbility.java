/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Issei Aoki
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.yuyh.library.imgsel.single;

import com.yuyh.library.imgsel.ResourceTable;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.DataAbilityHelper;
import ohos.aafwk.ability.DataAbilityRemoteException;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Image;
import ohos.media.image.ImageSource;
import ohos.utils.net.Uri;

import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.util.concurrent.ExecutorService;

public class ResultAbility extends Ability {

    private static final String TAG = ResultAbility.class.getSimpleName();
    public Image image;
    private ExecutorService mExecutor;

    public static Intent createIntent(Uri uri) {
        Intent intent = new Intent();
        intent.setParam("uri",uri);
        Operation operation = new Intent.OperationBuilder()
                .withBundleName("com.yuyh.imgsel")
                .withAbilityName(ResultAbility.class.getName())
                .build();
        intent.setOperation(operation);
        return intent;
    }

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
//        super.setMainRoute(ResultAbilitySlice.class.getName());
        super.setUIContent(ResourceTable.Layout_ability_result);
        image = (Image) findComponentById(ResourceTable.Id_result_image);
        final Uri uri = getIntent().getSequenceableParam("uri");
        DataAbilityHelper helper = DataAbilityHelper.creator(getContext());
        try {
            FileDescriptor fd = helper.openFile(uri,"r");
            ImageSource imageSource = ImageSource.create(fd,null);
            image.setPixelMap(imageSource.createPixelmap(null));
        } catch (DataAbilityRemoteException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

}
