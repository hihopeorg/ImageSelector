package com.yuyh.library.imgsel.ui;


import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.yuyh.library.imgsel.ISNav;
import com.yuyh.library.imgsel.config.ISListConfig;
import com.yuyh.library.imgsel.ResourceTable;
import com.yuyh.library.imgsel.adapter.FolderListAdapter;
import com.yuyh.library.imgsel.adapter.ImageListAdapter;
import com.yuyh.library.imgsel.adapter.ImageListAdapter.OnImageItemClickListener;
import com.yuyh.library.imgsel.bean.Folder;
import com.yuyh.library.imgsel.bean.MyImage;
import com.yuyh.library.imgsel.config.ISCameraConfig;
import com.yuyh.library.imgsel.utils.FileUtils;
import com.yuyh.library.imgsel.widget.CustomViewPager;
import com.yuyh.library.imgsel.widget.DividerGridItemDecoration;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorGroup;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.components.*;
import ohos.agp.utils.Color;
import ohos.agp.window.dialog.BaseDialog;
import ohos.agp.window.dialog.PopupDialog;
import ohos.app.Context;
import ohos.bundle.IBundleManager;
import ohos.security.SystemPermission;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;


public class ImgSelFragment extends ISListActivity implements
        ISNav.OnImagesLoadedListener,
        OnImageItemClickListener,
        ISListConfig.OnImageSelectedListener,
        Component.ClickedListener {

    public static final int REQUEST_PERMISSION_STORAGE = 0x01;
    public static final int REQUEST_PERMISSION_CAMERA = 0x02;
    public static final String EXTRAS_TAKE_PICKERS = "TAKE";
    public static final String EXTRAS_IMAGES = "IMAGES";

    private ISListConfig ISListConfig;


    private Component mFooterBar;     //底部栏
    private Button mBtnOk;       //确定按钮
    private Image mBtnBack;       //确定按钮
    private Component mllDir; //文件夹切换按钮
    private Text mtvDir; //显示当前文件夹
    private FolderListAdapter mFolderListAdapter;    //图片文件夹的适配器
     FolderPopUpWindow mFolderPopupWindow;  //ImageSet的PopupWindow
    public List<Folder> mFolders;   //所有的图片文件夹

    private boolean directPhoto = false; // 默认不是直接调取相机
    private ListContainer listContainer;
    private ImageListAdapter listProvider;

    private void initsListConfig() {
        if (ISListConfig.isMultiMode()==true){
            ISListConfig imageselect = ISListConfig.getInstance();
            imageselect.multiSelect(true);
        }else {
            ISListConfig imageselect = ISListConfig.getInstance();
            imageselect.multiSelect(false);
        }
    }


    @Override
    protected void onStart(Intent data){
        super.onStart(data);
        setUIContent(ResourceTable.Layout_ability_image_grid);
        ImageLoaderConfiguration config = ImageLoaderConfiguration.createDefault(this);
        ImageLoader.getInstance().init(config);     //UniversalImageLoader初始化
        ISListConfig.setTakePhotoAbility(ISCameraConfig.class);
        ISListConfig = ISListConfig.getInstance();
        ISListConfig.clear();
        ISListConfig.addOnImageSelectedListener(this);
        initsListConfig();
        // 新增可直接拍照
        if (data != null && data.getParams() != null) {
            directPhoto = data.getBooleanParam(EXTRAS_TAKE_PICKERS, false); // 默认- 不是直接打开相机
            if (directPhoto) {
                if (verifySelfPermission(SystemPermission.CAMERA) != IBundleManager.PERMISSION_GRANTED) {
                    requestPermissionsFromUser(new String[]{SystemPermission.CAMERA}, ImgSelFragment.REQUEST_PERMISSION_CAMERA);
                }else {
                    takePic();
                }
            }
            ArrayList<MyImage> images = (ArrayList<MyImage>) data.getSerializableParam(EXTRAS_IMAGES);
            ISListConfig.setSelectedImages(images);
        }

        listContainer = (ListContainer) findComponentById(ResourceTable.Id_recycler);
        TableLayoutManager tableLayoutManager = new TableLayoutManager();
        tableLayoutManager.setColumnCount(3);
        listContainer.setLayoutManager(tableLayoutManager);

        mBtnBack = (Image) findComponentById(ResourceTable.Id_btn_back);
        mBtnBack.setClickedListener(this);

        mBtnOk = (Button) findComponentById(ResourceTable.Id_btn_ok);
        mBtnOk.setClickedListener(this);

        mFooterBar = findComponentById(ResourceTable.Id_footer_bar);
        mllDir = findComponentById(ResourceTable.Id_ll_dir);
        mllDir.setClickedListener(this);
        mtvDir = (Text) findComponentById(ResourceTable.Id_tv_dir);
        if (ISListConfig.isMultiMode()) {
            mBtnOk.setVisibility(Component.VISIBLE);
        } else {
            mBtnOk.setVisibility(Component.HIDE);
        }

        mFolderListAdapter = new FolderListAdapter(this, null);
        listProvider = new ImageListAdapter(this, null);

        onImageSelected(0, null, false);

        if (verifySelfPermission(SystemPermission.READ_USER_STORAGE) == IBundleManager.PERMISSION_GRANTED &&
                verifySelfPermission(SystemPermission.READ_MEDIA) == IBundleManager.PERMISSION_GRANTED &&
                verifySelfPermission(SystemPermission.MEDIA_LOCATION) == IBundleManager.PERMISSION_GRANTED) {
            new ISNav(this, null, this);
        } else {
            requestPermissionsFromUser(new String[]{SystemPermission.READ_USER_STORAGE, SystemPermission.READ_MEDIA, SystemPermission.MEDIA_LOCATION}, REQUEST_PERMISSION_STORAGE);
        }
    }

    private void takePic() {
        ISListConfig.takePicture(this);
    }

    @Override
    public void onRequestPermissionsFromUserResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsFromUserResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_PERMISSION_STORAGE) {
            if (grantResults.length > 0 && grantResults[0] == IBundleManager.PERMISSION_GRANTED) {
                new ISNav(this, null, this);
            } else {
                showToast("权限被禁止，无法选择本地图片");
            }
        } else if (requestCode == REQUEST_PERMISSION_CAMERA) {
            if (grantResults.length > 0 && grantResults[0] == IBundleManager.PERMISSION_GRANTED) {
                takePic();
            } else {
                showToast("权限被禁止，无法打开相机");
            }
        }
    }

    @Override
    protected void onStop() {
        ISListConfig.removeOnImageSelectedListener(this);
        super.onStop();
    }

    @Override
    public void onClick(Component v) {
        int id = v.getId();
        if (id == ResourceTable.Id_btn_ok) {
            Intent intent = new Intent();
            intent.setParam(ISListConfig.EXTRA_RESULT_ITEMS, ISListConfig.getSelectedImages());
            setResult(ISListConfig.RESULT_CODE_ITEMS, intent);  //多选不允许裁剪裁剪，返回数据
            terminateAbility();
        } else if (id == ResourceTable.Id_ll_dir) {
            if (mFolders == null) {
                return;
            }
            //点击文件夹按钮
            createPopupFolderList();
            mFolderListAdapter.refreshData(mFolders);  //刷新数据
            if (mFolderPopupWindow.isShowing()) {
                mFolderPopupWindow.hide();
            } else {
                mFolderPopupWindow.showOnCertainPosition(1, (int) mFooterBar.getContentPositionX(), (int) mFooterBar.getContentPositionY());
                //默认选择当前选择的上一个，当目录很多时，直接定位到已选中的条目
                int index = mFolderListAdapter.getSelectIndex();
                index = index == 0 ? index : index - 1;
                mFolderPopupWindow.setSelection(index);
            }
        } else if (id == ResourceTable.Id_btn_back) {
            //点击返回按钮
            terminateAbility();
        }
    }

    /**
     * 创建弹出的窗口//
     */
    private void createPopupFolderList() {
        mFolderPopupWindow = new FolderPopUpWindow(this, mFolderListAdapter);
        this.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(ListContainer listContainer, Component view, int position, long l) {
                mFolderListAdapter.setSelectIndex(position);
                ISListConfig.setCurrentImageFolderPosition(position);
                mFolderPopupWindow.hide();
                Folder folder = (Folder) listContainer.getItemProvider().getItem(position);
                if (null != folder) {
                    listProvider.refreshData(folder.images);
                    mtvDir.setText(folder.name);
                }
            }
        });
        mFolderPopupWindow.setMargin(mFooterBar.getHeight());
    }






    @Override
    public void onImagesLoaded(List<Folder> folders) {

        this.mFolders = folders;
        ISListConfig.setImageFolders(folders);
        if (folders.size() == 0) {
            listProvider.refreshData(null);
        } else {
            listProvider.refreshData(folders.get(0).images);
        }
        listProvider.setOnImageItemClickListener(this);
        listContainer.setItemProvider(listProvider);
        mFolderListAdapter.refreshData(folders);
    }

    @Override
    public void onImageItemClick(Component view, MyImage myImage, int position) {
        //根据是否有相机按钮确定位置
        position = ISListConfig.isShowCamera() ? position - 1 : position;
        if (ISListConfig.isMultiMode()) {
            Intent intent = new Intent();
            Operation operation = new Intent.OperationBuilder()
                    .withDeviceId("")
                    .withBundleName(getBundleName())
                    .withAbilityName(CustomViewPager.class.getName())
                    .build();
            intent.setOperation(operation);
            intent.setParam(ISListConfig.EXTRA_SELECTED_IMAGE_POSITION, position);


            DividerGridItemDecoration.DataHolder.getInstance().save(DividerGridItemDecoration.DataHolder.DH_CURRENT_IMAGE_FOLDER_ITEMS, ISListConfig.getCurrentImageFolderItems());

            startAbilityForResult(intent, ISListConfig.REQUEST_CODE_PREVIEW);  //如果是多选，点击图片进入预览界面
        } else {

            ISListConfig.clearSelectedImages();
            ISListConfig.addSelectedImageItem(position, ISListConfig.getCurrentImageFolderItems().get(position), true);

                Intent intent = new Intent();
                intent.setParam(ISListConfig.EXTRA_RESULT_ITEMS, ISListConfig.getSelectedImages());
                setResult(ISListConfig.RESULT_CODE_ITEMS, intent);   //单选不需要裁剪，返回数据
                terminateAbility();
        }
    }

    @Override
    public void onImageSelected(int position, MyImage item, boolean isAdd) {
        if (ISListConfig.getSelectImageCount() > 0) {
            mBtnOk.setText(FileUtils.getString(this, ResourceTable.String_ip_select_complete, ISListConfig.getSelectImageCount(), ISListConfig.getSelectLimit()));
            mBtnOk.setEnabled(true);
            mBtnOk.setTextColor(new Color(FileUtils.getColor(this, ResourceTable.Color_ip_text_primary_inverted)));
        } else {
            mBtnOk.setText(FileUtils.getString(this, ResourceTable.String_ip_complete));
            mBtnOk.setEnabled(false);
            mBtnOk.setTextColor(new Color(FileUtils.getColor(this, ResourceTable.Color_ip_text_secondary_inverted)));
        }

        for (int i = ISListConfig.isShowCamera() ? 1 : 0; i < listProvider.getCount(); i++) {
            if (listProvider.getItem(i).uriSchema != null && listProvider.getItem(i).uriSchema.equals(item.uriSchema)) {
                listProvider.notifyDataSetItemChanged(i);
                return;
            }
        }
    }

    @Override
    protected void onAbilityResult(int requestCode, int resultCode, Intent data) {
        try {
            super.onAbilityResult(requestCode, resultCode, data);

            if (data != null && data.getParams() != null) {

                    //从拍照界面返回
                    //点击 X , 没有选择照片
                    if (data.getSerializableParam(ISListConfig.EXTRA_RESULT_ITEMS) == null) {
                        //什么都不做 直接调起相机
                        terminateAbility();
                    } else {
                        //说明是从裁剪页面过来的数据，直接返回就可以
                        setResult(ISListConfig.RESULT_CODE_ITEMS, data);
                        terminateAbility();
                    }


            } else {

                //如果是裁剪，因为裁剪指定了存储的Uri，所以返回的data一定为null
                if (resultCode == ISListConfig.RESULT_OK && requestCode == ISListConfig.REQUEST_CODE_TAKE) {

                    //发送广播通知图片增加了
//                sListConfig.galleryAddPic(this, sListConfig.getTakeImageFile());


                    String path = ISListConfig.getTakeImageFile().getAbsolutePath();

                    MyImage myImage = new MyImage();
                    myImage.uriSchema = path;
                    ISListConfig.clearSelectedImages();
                    ISListConfig.addSelectedImageItem(0, myImage, true);

                        Intent intent = new Intent();
                        intent.setParam(ISListConfig.EXTRA_RESULT_ITEMS, ISListConfig.getSelectedImages());
                        setResult(ISListConfig.RESULT_CODE_ITEMS, intent);   //单选不需要裁剪，返回数据
                        terminateAbility();

                } else if (directPhoto) {
                    terminateAbility();
                }
            }
        }catch (Exception e){
            terminateAbility();
        }

    }



    private OnItemClickListener onItemClickListener;

public class FolderPopUpWindow extends PopupDialog implements Component.ClickedListener, BaseDialog.DialogListener {

    private ListContainer listView;
    private final Component masker;
    private final Component marginView;
    private int marginPx;

    public FolderPopUpWindow(Context context, BaseItemProvider provider) {
        super(context, null);
        LayoutScatter scatter = LayoutScatter.getInstance(context);
        final Component component = scatter.parse(ResourceTable.Layout_pop_folder, null, false);
        masker = component.findComponentById(ResourceTable.Id_masker);
        masker.setClickedListener(this);
        marginView = component.findComponentById(ResourceTable.Id_margin);
        marginView.setClickedListener(this);
        listView = (ListContainer) component.findComponentById(ResourceTable.Id_listView);
        listView.setItemProvider(provider);

        setCustomComponent(component);
        //如果不设置，就是 AnchorView 的宽度
        ComponentContainer.LayoutConfig layoutConfig =new ComponentContainer.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT,
                900);
        listView.setLayoutConfig(layoutConfig);
        listView.setFocusable(Component.FOCUS_ENABLE);
        setBackColor(new Color(0));

        //setAnimationStyle(0);
        component.getComponentTreeObserver().addTreeLayoutChangedListener(new ComponentTreeObserver.GlobalLayoutListener() {
            @Override
            public void onGlobalLayoutUpdated() {
                component.getComponentTreeObserver().removeTreeLayoutChangedListener(this);
                int maxHeight = component.getHeight() * 5 / 8;
                int realHeight = listView.getHeight();
                ComponentContainer.LayoutConfig listParams = listView.getLayoutConfig();
                listParams.height = realHeight > maxHeight ? maxHeight : realHeight;
                listView.setLayoutConfig(listParams);
                DirectionalLayout.LayoutConfig marginParams = (DirectionalLayout.LayoutConfig) marginView.getLayoutConfig();
                marginParams.height = marginPx;
                marginView.setLayoutConfig(marginParams);
                enterAnimator();
            }

        });
        listView.setItemClickedListener(new ListContainer.ItemClickedListener() {
            @Override
            public void onItemClicked(ListContainer listContainer, Component component, int position, long l) {
                if (onItemClickListener != null) onItemClickListener.onItemClick(listContainer, component, position, l);
            }

        });
    }


    private void enterAnimator() {
        AnimatorProperty alpha = masker.createAnimatorProperty();
        try {
            Method alphaFrom = AnimatorProperty.class.getDeclaredMethod("alphaFrom", float.class);
            alphaFrom.setAccessible(true);
            alphaFrom.invoke(alpha, 0);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            alpha.alphaFrom(0);
        }
        alpha.alpha(1);
//        ObjectAnimator alpha = ObjectAnimator.ofFloat(masker, "alpha", 0, 1);
        AnimatorProperty translationY = listView.createAnimatorProperty().moveFromY(listView.getHeight()).moveToY(0);
        AnimatorGroup group = new AnimatorGroup();
        group.setDuration(400);
        group.setCurveType(Animator.CurveType.ACCELERATE_DECELERATE);
        group.runParallel(alpha,translationY);
        group.start();
    }


    @Override
    protected void onHide() {
        exitAnimator();
        super.onHide();
    }

    private void exitAnimator() {
//        AnimatorProperty  alpha = masker.createAnimatorProperty().alphaFrom(1).alpha(0);
        AnimatorProperty alpha = masker.createAnimatorProperty();
        try {
            Method alphaFrom = AnimatorProperty.class.getDeclaredMethod("alphaFrom", float.class);
            alphaFrom.setAccessible(true);
            alphaFrom.invoke(alpha, 1);
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            alpha.alphaFrom(0);
        }
        alpha.alpha(0);
        AnimatorProperty translationY = listView.createAnimatorProperty().moveFromY(0).moveToY(listView.getHeight());

        AnimatorGroup group = new AnimatorGroup();
        group.setDuration(300);
        group.runParallel(alpha,translationY);
        group.setCurveType(Animator.CurveType.ACCELERATE_DECELERATE);
        group.setStateChangedListener(new Animator.StateChangedListener() {
            @Override
            public void onStart(Animator animator) {
                listView.setVisibility(Component.VISIBLE);
            }

            @Override
            public void onStop(Animator animator) {

            }

            @Override
            public void onCancel(Animator animator) {

            }

            @Override
            public void onEnd(Animator animator) {
                FolderPopUpWindow.super.hide();
            }

            @Override
            public void onPause(Animator animator) {

            }

            @Override
            public void onResume(Animator animator) {

            }
        });
        group.start();

    }



    public void setSelection(int selection) {
        listView.setSelectedItemIndex(selection);
    }

    public void setMargin(int marginPx) {
        this.marginPx = marginPx;
    }

    @Override
    public void onClick(Component v) {
        hide();
    }

    @Override
    public boolean isTouchOutside() {
        return true;
    }


}
    public void setOnItemClickListener(OnItemClickListener listener) {
        this.onItemClickListener = listener;
    }
    public interface OnItemClickListener {
        void onItemClick(ListContainer listContainer, Component view, int position, long l);
    }
}