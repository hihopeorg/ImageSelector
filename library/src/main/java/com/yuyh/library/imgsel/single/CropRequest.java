/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Issei Aoki
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.yuyh.library.imgsel.single;


import com.yuyh.library.imgsel.single.callback.CropCallback;
import ohos.utils.net.Uri;

public class CropRequest {

  private CropImageView cropImageView;
  private Uri sourceUri;
  private int outputWidth;
  private int outputHeight;
  private int outputMaxWidth;
  private int outputMaxHeight;

  public CropRequest(CropImageView cropImageView, Uri sourceUri) {
    this.cropImageView = cropImageView;
    this.sourceUri = sourceUri;
  }

  public CropRequest outputWidth(int outputWidth) {
    this.outputWidth = outputWidth;
    this.outputHeight = 0;
    return this;
  }

  public CropRequest outputHeight(int outputHeight) {
    this.outputHeight = outputHeight;
    this.outputWidth = 0;
    return this;
  }

  public CropRequest outputMaxWidth(int outputMaxWidth) {
    this.outputMaxWidth = outputMaxWidth;
    return this;
  }

  public CropRequest outputMaxHeight(int outputMaxHeight) {
    this.outputMaxHeight = outputMaxHeight;
    return this;
  }

  private void build() {
    if (outputWidth > 0) cropImageView.setOutputWidth(outputWidth);
    if (outputHeight > 0) cropImageView.setOutputHeight(outputHeight);
    cropImageView.setOutputMaxSize(outputMaxWidth, outputMaxHeight);
  }

  public void execute(CropCallback cropCallback) {
    build();
    cropImageView.cropAsync(sourceUri, cropCallback);
  }

}
